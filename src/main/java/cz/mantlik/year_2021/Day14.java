package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class Day14 extends AbstractDay {

	private String template = "";
	private final Map<String, String> rules = new HashMap<>();
	private final Map<Character, Long> characterCounts = new HashMap<>();

	private final Map<String, Pair<String, String>> newRules = new HashMap<>();

	public Day14() {
		super(true);
	}

	@Override
	protected void processLineFirstPart(String input) {
		processLine(input, (key, value) -> rules.put(key, key.charAt(0) + value));
	}

	private void processLine(String input, BiConsumer<String, String> ruleMaker) {
		if (template.isEmpty()) {
			template = input;
		} else if (!input.isEmpty()) {
			String[] inputParts = input.split(" -> ");
			ruleMaker.accept(inputParts[0], inputParts[1]);
		}
	}

	@Override
	protected void finishFirstPart() {
		int steps = 10;
		for (int i = 0; i < steps; i++) {
			applyRules();
		}

		finish(steps);
	}

	@Override
	protected void setupSecondPart() {
		template = "";
		characterCounts.clear();
	}

	private void applyRules() {
		StringBuilder templateBuilder = new StringBuilder();
		for (int j = 0; j < template.length() - 1; j++) {
			String pair = template.substring(j, j + 2);
			pair = rules.containsKey(pair) ? rules.get(pair) : pair.substring(0, 1);
			templateBuilder.append(pair);
		}
		templateBuilder.append(template.charAt(template.length() - 1));
		template = templateBuilder.toString();
	}

	private void finish(int steps) {
		template.chars().mapToObj(i -> (char) i).forEach(this::incrementCounter);
		List<Long> counts = characterCounts.values().stream().sorted().collect(Collectors.toList());
		Long result = counts.get(counts.size() - 1) - counts.get(0);

		LOG.info("After step {0}, the polymer has length {1}.", steps, characterCounts.values().stream().reduce(Long::sum).orElse(0L));
		LOG.info("Taking the quantity of the most common element and subtracting the quantity of the least " +
				"common element produces {0}.", result);
	}

	private void incrementCounter(char key) {
		long count = characterCounts.getOrDefault(key, 0L) + 1;
		characterCounts.put(key, count);
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLine(input, (key, value) -> {
			String firstReplacement = key.charAt(0) + value;
			String secondReplacement = value + key.charAt(1);
			newRules.put(key, Pair.of(firstReplacement, secondReplacement));
		});
	}

	@Override
	protected void finishSecondPart() {
		int steps = 40;
		for (int j = 0; j < template.length() - 1; j++) {
			String value = template.substring(j, j + 2);
			replace(value, steps);
		}
		finish(steps);
	}

	private void replace(String value, int step) {
		if (step < 1) {
			return;
		}
		Pair<String, String> newValues = newRules.get(value);
		if (newValues == null) {
			return;
		}

		incrementCounter(newValues.getRight().charAt(0));
		replace(newValues.getLeft(), step - 1);
		replace(newValues.getRight(), step - 1);
	}
}
