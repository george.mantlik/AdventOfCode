package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.*;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;

public class Day4 extends AbstractDay {

	private final List<Integer> drawnNumbers = new ArrayList<>();
	private final List<Integer> currentBoard = new ArrayList<>();
	private final Map<Integer, Integer> scores = new HashMap<>();

	public Day4() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		if (drawnNumbers.isEmpty()) {
			drawnNumbers.addAll(Arrays.stream(input.split(",")).map(Integer::parseInt).collect(Collectors.toList()));
		} else if (input.isEmpty()) {
			if (currentBoard.isEmpty()) {
				return;
			}
			List<Integer> drawnIndexes = drawnNumbers.stream().map(currentBoard::indexOf).collect(Collectors.toList());
			int winningIndex = Math.min(Math.max(0, wholeRowIn(drawnIndexes)), Math.max(0, wholeColIn(drawnIndexes)));
			int undrawnNumbersCount = currentBoard.stream().filter(n -> !drawnNumbers.subList(0, winningIndex + 1).contains(n)).reduce(0, Integer::sum);
			scores.put(winningIndex, undrawnNumbersCount * drawnNumbers.get(winningIndex));
			currentBoard.clear();
		} else {
			currentBoard.addAll(Arrays.stream(input.split(" ")).filter(s -> !s.isEmpty()).map(Integer::parseInt).collect(Collectors.toList()));
		}
	}

	private int wholeRowIn(List<Integer> drawnIndexes) {
		return wholeLineIn(drawnIndexes, i -> i / 5);
	}

	private int wholeColIn(List<Integer> drawnIndexes) {
		return wholeLineIn(drawnIndexes, i -> i % 5);
	}

	private int wholeLineIn(List<Integer> drawnIndexes, IntUnaryOperator indexModifier) {
		List<Integer> wholeLineCounters = Arrays.asList(0, 0, 0, 0, 0);
		for (int i = 0; i < drawnIndexes.size(); i++) {
			int drawnIndex = drawnIndexes.get(i);
			if (drawnIndex < 0) {
				continue;
			}
			int counterIndex = indexModifier.applyAsInt(drawnIndex);
			wholeLineCounters.set(counterIndex, wholeLineCounters.get(counterIndex) + 1);
			if (wholeLineCounters.stream().anyMatch(v -> v == 5)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	protected void finishFirstPart() {
		int winningScore = scores.entrySet().stream().sorted(Comparator.comparingInt(Map.Entry::getKey)).map(Map.Entry::getValue).findFirst().orElse(-1);
		LOG.info("The score of the winning board is {0}.", winningScore);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Nothing to do
	}

	@Override
	protected void finishSecondPart() {
		int lastScore = scores.entrySet().stream()
				.sorted(Comparator.comparingInt(Map.Entry::getKey))
				.map(Map.Entry::getValue)
				.reduce((first, second) -> second)
				.orElse(-1);
		LOG.info("The score of the last board is {0}.", lastScore);
	}
}
