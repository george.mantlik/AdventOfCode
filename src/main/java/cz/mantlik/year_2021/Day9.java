package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class Day9 extends AbstractDay {

	private final Deque<List<Integer>> rows = new LinkedList<>();
	private int riskLevelSum = 0;

	private final Map<Integer, Basin> activeBasins = new HashMap<>();
	private final List<Integer> basinSizes = new ArrayList<>();

	public Day9() {
		super(false);
		rows.add(new ArrayList<>());
	}

	@Override
	protected void processLineFirstPart(String input) {
		rows.add(Arrays.stream(input.split("")).map(Integer::parseInt).collect(Collectors.toList()));
		findRiskPoints();
	}

	private void findRiskPoints() {
		if (rows.size() > 2) {
			List<Integer> prevRow = rows.pollFirst();
			List<Integer> thisRow = rows.peekFirst();
			List<Integer> nextRow = rows.peekLast();
			for (int i = 0; i < thisRow.size(); i++) {
				if (neighborRowBigger(thisRow, prevRow, i) && neighborRowBigger(thisRow, nextRow, i)
						&& neighborColBigger(thisRow, i, -1) && neighborColBigger(thisRow, i, 1)) {
					riskLevelSum += thisRow.get(i) + 1;
				}
			}
		}
	}

	private boolean neighborColBigger(List<Integer> thisRow, int index, int diff) {
		int neighborIndex = index + diff;
		boolean neighborOutOfBounds = neighborIndex < 0 || neighborIndex >= thisRow.size();
		return neighborOutOfBounds || thisRow.get(neighborIndex) > thisRow.get(index);
	}

	private boolean neighborRowBigger(List<Integer> thisRow, List<Integer> neighborRow, int index) {
		return neighborRow.isEmpty() || neighborRow.get(index) > thisRow.get(index);
	}

	@Override
	protected void finishFirstPart() {
		rows.add(new ArrayList<>());
		findRiskPoints();
		LOG.info("The sum of the risk levels of all low points in the heightmap is {0}.", riskLevelSum);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		List<Integer> points = Arrays.stream(input.split("")).map(Integer::parseInt).collect(Collectors.toList());

		int basinStart = 0;
		for (int i = 0; i < points.size(); i++) {
			if (points.get(i) == 9) {
				addIndexesToBasin(basinStart, i);
				removeIndexFromBasin(i);
				basinStart = i + 1;
			}
		}
		addIndexesToBasin(basinStart, points.size());
	}

	private void removeIndexFromBasin(int index) {
		Basin basin = activeBasins.remove(index);
		if (basin == null) {
			return;
		}

		basin.indexes.remove(index);
		if (basin.indexes.isEmpty()) {
			basinSizes.add(basin.size);
		}
	}

	private void addIndexesToBasin(int start, int end) {
		Basin basin = findBasin(start, end);

		for (int i = start; i < end; i++) {
			activeBasins.put(i, basin);
			basin.indexes.add(i);
			basin.size += 1;
		}
	}

	private Basin findBasin(int start, int end) {
		List<Basin> basins = activeBasins.entrySet().stream().filter(e -> e.getKey() >= start && e.getKey() < end).map(Map.Entry::getValue).distinct().collect(Collectors.toList());
		Basin basin = basins.isEmpty() ? new Basin() : basins.get(0);
		for (int i = 1; i < basins.size(); i++) {
			basin.size += basins.get(i).size;
			basin.indexes.addAll(basins.get(i).indexes);
		}
		return basin;
	}

	@Override
	protected void finishSecondPart() {
		int greatestIndex = activeBasins.keySet().stream().max(Integer::compareTo).orElse(-1);
		processLineSecondPart(StringUtils.leftPad("", greatestIndex + 1, '9'));

		int basinSizesMultiplication = basinSizes.stream().sorted().skip(basinSizes.size() - 3L).reduce(1, (a, b) -> a * b);
		LOG.info("Multiplying sizes of the three largest basins gives {0}.", basinSizesMultiplication);
	}

	private static class Basin {

		private final Set<Integer> indexes = new HashSet<>();
		private int size = 0;

	}
}
