package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day21 extends AbstractDay {

	private final List<Player> players = new ArrayList<>();
	private int dieRolls = 0;
	private final Map<Integer, Long> rollUniverseCounts = new HashMap<>();

	public Day21() {
		super(false);
		rollUniverseCounts.put(3, 1L);
		rollUniverseCounts.put(4, 3L);
		rollUniverseCounts.put(5, 6L);
		rollUniverseCounts.put(6, 7L);
		rollUniverseCounts.put(7, 6L);
		rollUniverseCounts.put(8, 3L);
		rollUniverseCounts.put(9, 1L);
	}

	@Override
	protected void processLineFirstPart(String input) {
		String[] inputParts = input.split(" starting position: ");
		players.add(new Player(players.size(), Integer.parseInt(inputParts[1])));
	}

	@Override
	protected void finishFirstPart() {
		while (true) {
			int steps = rollDie() + rollDie() + rollDie();
			Player player = players.get((dieRolls + 1) % players.size());
			if (player.move(steps) >= 1000) {
				break;
			}
		}
		int minScore = players.stream().map(p -> p.score).min(Integer::compareTo).orElse(0);
		int result = dieRolls * minScore;
		LOG.info("Multiplying the score of the losing player ({0}) by the number of times the die was rolled during the game ({1}) gives {2}.", minScore, dieRolls, result);
	}

	@Override
	protected void setupSecondPart() {
		players.clear();
	}

	private int rollDie() {
		int roll = ++dieRolls % 100;
		return roll > 0 ? roll : 100;
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLineFirstPart(input);
	}

	@Override
	protected void finishSecondPart() {
		playerRoll(players.get(0), players.get(1), 1);
		LOG.info("The player that wins in most universes wins in {0} universes.", players.stream().map(p -> p.wins).max(Long::compareTo).orElse(0L));
	}

	private void playerRoll(Player playerOnTurn, Player otherPlayer, long universeCount) {
		for (int i = 3; i <= 9; i++) {
			Player newPlayer = new Player(playerOnTurn);
			long newUniverseCount = rollUniverseCounts.get(i) * universeCount;
			if (newPlayer.move(i) >= 21) {
				players.get(newPlayer.id).wins += newUniverseCount;
			} else {
				playerRoll(otherPlayer, newPlayer, newUniverseCount);
			}
		}
	}

	private static class Player {

		private final int id;
		private int position;
		private int score = 0;
		private long wins = 0;

		public Player(int id, int position) {
			this.id = id;
			this.position = position;
		}

		public Player(Player other) {
			this.id = other.id;
			this.position = other.position;
			this.score = other.score;
		}

		public int move(int steps) {
			position = (position + steps) % 10;
			score += position > 0 ? position : 10;
			return score;
		}

		@Override
		public String toString() {
			return "Player " + id +
					"(pos=" + position +
					", score=" + score +
					')';
		}
	}
}
