package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.List;

public class Day3 extends AbstractDay {

	private final List<Integer> countsOfOnes = new ArrayList<>();
	private int inputSize = 0;
	private final List<Character> mostCommon = new ArrayList<>();

	private final List<List<String>> oxygenCandidates = new ArrayList<>();
	private final List<List<String>> co2Candidates = new ArrayList<>();

	public Day3() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		while (countsOfOnes.size() < input.length()) {
			countsOfOnes.add(0);
		}

		for (int i = 0; i < input.length(); i++) {
			if (input.charAt(i) == '1') {
				countsOfOnes.set(i, countsOfOnes.get(i) + 1);
			}
		}
		inputSize += 1;
	}

	@Override
	protected void finishFirstPart() {
		for (Integer countOfOnes : countsOfOnes) {
			mostCommon.add(2 * countOfOnes >= inputSize ? '1' : '0');
		}

		int gamma = 0;
		int epsilon = 0;
		for (int i = 0; i < mostCommon.size(); i++) {
			int addition = 1 << i;
			if (mostCommon.get(mostCommon.size() - 1 - i) == '1') {
				gamma += addition;
			} else {
				epsilon += addition;
			}
		}

		int powerConsumption = gamma * epsilon;
		LOG.info("Multiplying the gamma rate ({0}) by the epsilon rate ({1}) produces the power consumption, {2}.", gamma, epsilon, powerConsumption);
	}

	@Override
	protected void setupSecondPart() {
		while (oxygenCandidates.size() < mostCommon.size()) {
			oxygenCandidates.add(new ArrayList<>());
			co2Candidates.add(new ArrayList<>());
		}
	}

	@Override
	protected void processLineSecondPart(String input) {
		if (input.charAt(0) == mostCommon.get(0)) {
			oxygenCandidates.get(0).add(input);
		} else {
			co2Candidates.get(0).add(input);
		}
	}

	@Override
	protected void finishSecondPart() {
		int oxygen = filterCandidates(oxygenCandidates, 1, true);
		int co2 = filterCandidates(co2Candidates, 1, false);
		int lifeSupport = oxygen * co2;
		LOG.info("Multiplying the oxygen generator rating ({0}) by the CO2 Scrubber rating ({1}) produces the life support rating, {2}.", oxygen, co2, lifeSupport);
	}

	private int filterCandidates(List<List<String>> list, int index, boolean takeMost) {
		List<String> currentCandidates = list.get(index - 1);
		if (index > list.size() || currentCandidates.size() < 2) {
			return Integer.parseInt(currentCandidates.get(0), 2);
		}
		long countOfOnes = currentCandidates.stream().filter(s -> s.charAt(index) == '1').count();
		boolean mostOnes = 2 * countOfOnes >= currentCandidates.size();
		boolean takeOne = (mostOnes && takeMost) || (!mostOnes && !takeMost);

		char wantedCharacter = takeOne ? '1' : '0';

		for (String input : list.get(index - 1)) {
			if (input.charAt(index) == wantedCharacter) {
				list.get(index).add(input);
			}
		}
		return filterCandidates(list, index + 1, takeMost);
	}
}
