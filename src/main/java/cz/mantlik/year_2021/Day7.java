package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;

public class Day7 extends AbstractDay {

	private final List<Integer> fuelCounts = new ArrayList<>();

	public Day7() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		processLine(input, moves -> moves);
	}

	@Override
	protected void finishFirstPart() {
		int leastFuel = fuelCounts.stream().sorted().findFirst().orElse(0);
		LOG.info("Crab alignment costs a total of {0} fuel.", leastFuel);
	}

	@Override
	protected void setupSecondPart() {
		fuelCounts.clear();
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLine(input, moves -> (moves + 1) * moves / 2);
	}

	@Override
	protected void finishSecondPart() {
		finishFirstPart();
	}

	private void processLine(String input, IntUnaryOperator fuelCounter) {
		List<Integer> crabPositions = Arrays.stream(input.split(",")).map(Integer::parseInt).sorted().collect(Collectors.toList());
		for (int i = 0; i < crabPositions.size(); i++) {
			for (int j = crabPositions.get(0); j <= crabPositions.get(crabPositions.size() - 1); j++) {
				int index = j - crabPositions.get(0);
				if (fuelCounts.size() <= index) {
					fuelCounts.add(0);
				}
				int moves = Math.abs(crabPositions.get(i) - j);
				int count = fuelCounter.applyAsInt(moves);
				fuelCounts.set(index, count + fuelCounts.get(index));
			}
		}
	}
}
