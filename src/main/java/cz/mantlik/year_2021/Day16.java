package cz.mantlik.year_2021;

import org.apache.commons.lang3.StringUtils;
import cz.mantlik.common.AbstractDay;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class Day16 extends AbstractDay {

	private final List<String> hexTransmission = new ArrayList<>();
	private final Queue<Character> binaryTransmission = new LinkedList<>();
	private int hexIndex = 0;
	private Packet startPacket = null;

	public Day16() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		hexTransmission.addAll(Arrays.asList(input.split("")));
	}

	@Override
	protected void finishFirstPart() {
		loadPacket(null);
		LOG.info("Sum of all packet version numbers is {0}.", sumVersions(startPacket));
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	private long loadPacket(Packet parent) {
		int version = getInt(3);
		int type = getInt(3);

		Packet packet = new Packet(version, type);
		if (parent == null) {
			startPacket = packet;
		} else {
			parent.children.add(packet);
		}

		long bitCount = packet.type == PacketType.LITERAL ? loadLiteralPacket(packet) : loadOperatorPacket(packet);
		return bitCount + 6;
	}

	private long loadLiteralPacket(Packet packet) {
		long bitCount = 0L;
		int lastStart = 1;
		while (lastStart > 0) {
			lastStart = getInt(1);
			packet.value = packet.value * 16 + getInt(4);
			bitCount += 5;
		}
		return bitCount;
	}

	private long loadOperatorPacket(Packet packet) {
		long bitCount = 0L;
		int lengthBitCount = getInt(1) == 0 ? 15 : 11;

		int length = getInt(lengthBitCount);
		long counter = 0;
		while (counter < length) {
			bitCount += loadPacket(packet);
			counter += lengthBitCount == 15 ? bitCount - counter : 1;
		}

		return bitCount + lengthBitCount + 1;
	}

	private int getInt(int bitCount) {
		while (binaryTransmission.size() < bitCount) {
			String binaryString = StringUtils.leftPad(Integer.toBinaryString(Integer.parseInt(hexTransmission.get(hexIndex++), 16)), 4, "0");
			binaryTransmission.addAll(binaryString.chars().mapToObj(i -> (char) i).collect(Collectors.toList()));
		}
		StringBuilder bitsBuilder = new StringBuilder();
		for (int i = 0; i < bitCount; i++) {
			bitsBuilder.append(binaryTransmission.poll());
		}
		return Integer.parseInt(bitsBuilder.toString(), 2);
	}

	private int sumVersions(Packet packet) {
		return packet.version + packet.children.stream().map(this::sumVersions).reduce(Integer::sum).orElse(0);
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Nothing to do
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("The expression represented by this hexadecimal-encoded BITS transmission produces {0}.", evaluate(startPacket));
	}

	private long evaluate(Packet packet) {
		return packet.children.stream().map(this::evaluate).reduce(packet.type.operation).orElse(packet.value);
	}

	private static class Packet {

		private final int version;
		private final PacketType type;
		private final List<Packet> children = new ArrayList<>();
		private long value = 0;

		public Packet(int version, int type) {
			this.version = version;
			this.type = PacketType.values()[type];
		}
	}

	private enum PacketType {

		SUM(Long::sum),
		PRODUCT((first, second) -> first * second),
		MIN(Long::min),
		MAX(Long::max),
		LITERAL((first, second) -> first),
		GREATER(comparator(1)),
		LESS(comparator(-1)),
		EQUAL(comparator(0));

		private final BinaryOperator<Long> operation;

		PacketType(BinaryOperator<Long> operation) {
			this.operation = operation;
		}

		private static BinaryOperator<Long> comparator(long wantedSignum) {
			return (first, second) -> Long.signum(first.compareTo(second)) == wantedSignum ? 1L : 0;
		}
	}
}
