package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.*;
import java.util.stream.Collectors;

public class Day10 extends AbstractDay {

	private final Map<Integer, Integer> illegalCharacters = new HashMap<>();
	private int illegalPoints = 0;

	private final Map<Integer, Integer> completionCharacters = new HashMap<>();
	private final List<Long> completionPoints = new ArrayList<>();

	public Day10() {
		super(false);
		addIllegalCharacterPoints(')', 3);
		addIllegalCharacterPoints(']', 57);
		addIllegalCharacterPoints('}', 1197);
		addIllegalCharacterPoints('>', 25137);
		addCompletionCharacterPoints('(', 1);
		addCompletionCharacterPoints('[', 2);
		addCompletionCharacterPoints('{', 3);
		addCompletionCharacterPoints('<', 4);
	}

	private void addIllegalCharacterPoints(int character, int points) {
		illegalCharacters.put(character, points);
	}

	private void addCompletionCharacterPoints(int character, int points) {
		completionCharacters.put(character, points);
	}

	@Override
	protected void processLineFirstPart(String input) {
		input = deletePairs(input);
		OptionalInt error = findError(input);
		if (error.isPresent()) {
			illegalPoints += illegalCharacters.get(error.getAsInt());
		}
	}

	private String deletePairs(String input) {
		String prevInput = "";
		while (!prevInput.equals(input)) {
			prevInput = input;
			input = input.replaceAll("\\(\\)|\\[]|\\{}|<>", "");
		}
		return input;
	}

	private OptionalInt findError(String input) {
		return input.chars().filter(illegalCharacters::containsKey).findFirst();
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("The total syntax error score for this file is {0} points!", illegalPoints);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		input = deletePairs(input);
		if (!findError(input).isPresent()) {
			long points = 0;
			for (char character : new StringBuilder(input).reverse().toString().toCharArray()) {
				points = points * 5 + completionCharacters.get((int) character);
			}
			completionPoints.add(points);
		}
	}

	@Override
	protected void finishSecondPart() {
		int index = completionPoints.size() / 2;
		long middleScore = completionPoints.stream().sorted().collect(Collectors.toList()).get(index);
		LOG.info("The middle score for this file is {0} points!", middleScore);
	}
}
