package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;
import cz.mantlik.puzzleutils.Point;

import java.util.*;
import java.util.stream.Collectors;

public class Day5 extends AbstractDay {

	private final Map<Point, Integer> map = new HashMap<>();

	public Day5() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		List<Integer> coordinates = Arrays.stream(input.split(",| -> ")).map(Integer::parseInt).collect(Collectors.toList());
		if (!horizontalOrVertical(coordinates)) {
			return;
		}

		processCoordinates(coordinates);
	}

	private boolean horizontalOrVertical(List<Integer> coordinates) {
		return coordinates.get(0).equals(coordinates.get(2)) || coordinates.get(1).equals(coordinates.get(3));
	}

	private void processCoordinates(List<Integer> coordinates) {
		Integer x = coordinates.get(0);
		Integer endX = coordinates.get(2);
		Integer y = coordinates.get(1);
		Integer endY = coordinates.get(3);

		int diffX = endX.compareTo(x);
		int diffY = endY.compareTo(y);

		while (true) {
			Point coord = new Point(x, y);
			int count = map.getOrDefault(coord, 0);
			map.put(coord, count + 1);
			if (x.equals(endX) && y.equals(endY)) {
				break;
			}
			x += diffX;
			y += diffY;
		}
	}

	@Override
	protected void finishFirstPart() {
		long overlappingCount = map.entrySet().stream().filter(e -> e.getValue() > 1).count();
		LOG.info("Considering only horizontal and vertical lines, at least two lines overlap at {0} points.", overlappingCount);
	}

	@Override
	protected void setupSecondPart() {
		map.clear();
	}

	@Override
	protected void processLineSecondPart(String input) {
		List<Integer> coordinates = Arrays.stream(input.split(",| -> ")).map(Integer::parseInt).collect(Collectors.toList());
		processCoordinates(coordinates);
	}

	@Override
	protected void finishSecondPart() {
		long overlappingCount = map.entrySet().stream().filter(e -> e.getValue() > 1).count();
		LOG.info("Considering all lines, at least two lines overlap at {0} points.", overlappingCount);
	}
}
