package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;
import cz.mantlik.puzzleutils.Point;

import java.util.HashSet;
import java.util.Set;

public class Day22 extends AbstractDay {

	private final Set<Point> cubesOn = new HashSet<>();

	public Day22() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		String[] data = input.split(" x=|\\.\\.|,y=|,z=");
		Point start = new Point(data[1], data[3], data[5]);
		Point end = new Point(data[2], data[4], data[6]);

		if (filterCubes(start, end)) {
			for (int i = start.get(Point.X); i <= end.get(Point.X); i++) {
				for (int j = start.get(Point.Y); j <= end.get(Point.Y); j++) {
					for (int k = start.get(Point.Z); k <= end.get(Point.Z); k++) {
						Point point = new Point(i, j, k);
						if (data[0].equals("on")) {
							cubesOn.add(point);
						} else {
							cubesOn.remove(point);
						}
					}
				}
			}
		}
	}

	private boolean filterCubes(Point start, Point end) {
		return isInRange(start.get(0), end.get(0)) && isInRange(start.get(1), end.get(1)) && isInRange(start.get(2), end.get(2));
	}

	private boolean isInRange(int start, int end) {
		return (start >= -50 && start <= 50) || (end >= -50 && end <= 50);
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("Considering only cubes in the region x=-50..50,y=-50..50,z=-50..50, there are {0} cubes on.", cubesOn.size());
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {

	}

	@Override
	protected void finishSecondPart() {

	}
}
