package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.List;

public class Day1 extends AbstractDay {

	private int increasingMeasurementsCount = 0;
	private Integer previousDepth = null;

	private int increasingSumsCount = 0;
	private final List<Integer> sums = new ArrayList<>();

	public Day1() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		int depth = Integer.parseInt(input);
		if (previousDepth != null && previousDepth < depth) {
			increasingMeasurementsCount += 1;
		}
		previousDepth = depth;
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("There are {0} measurements that are larger than the previous measurement.", increasingMeasurementsCount);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		int depth = Integer.parseInt(input);

		Integer completeSum = sums.size() > 2 ? sums.remove(0) : null;

		for (int i = 0; i < sums.size(); i++) {
			int previousValue = sums.get(i);
			sums.set(i, previousValue + depth);
		}
		sums.add(depth);

		if (completeSum != null && completeSum < sums.get(0)) {
			increasingSumsCount += 1;
		}
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("There are {0} sums that are larger than the previous sum.", increasingSumsCount);
	}
}
