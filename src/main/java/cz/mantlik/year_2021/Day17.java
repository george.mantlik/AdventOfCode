package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;
import cz.mantlik.puzzleutils.MathUtils;
import cz.mantlik.puzzleutils.Point;

import java.util.function.IntBinaryOperator;

import static cz.mantlik.puzzleutils.Point.X;
import static cz.mantlik.puzzleutils.Point.Y;

public class Day17 extends AbstractDay {

	private Point topLeft;
	private Point bottomRight;

	public Day17() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		String[] targetArea = input.split("target area: x=|\\.\\.|, y=");
		topLeft = getPoint(targetArea, Math::min, Math::max);
		bottomRight = getPoint(targetArea, Math::max, Math::min);
	}

	private Point getPoint(String[] targetArea, IntBinaryOperator xOperator, IntBinaryOperator yOperator) {
		return new Point(getCoordinate(targetArea[1], targetArea[2], xOperator), getCoordinate(targetArea[3], targetArea[4], yOperator));
	}

	private int getCoordinate(String c1, String c2, IntBinaryOperator operator) {
		return operator.applyAsInt(Integer.parseInt(c1), Integer.parseInt(c2));
	}

	@Override
	protected void finishFirstPart() {
		int yMax = MathUtils.simpleArithmeticSeries(-bottomRight.get(Y) - 1);
		LOG.info("The highest position the y can reach and still eventually be within the target area after any step is {0}.", yMax);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Nothing to do
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("There are {0} distinct initial velocity values cause the probe to be within the target area after any step.", countVelocityPairs());
	}

	private int countVelocityPairs() {
		int count = 0;

		for (int yVelocity = -bottomRight.get(Y) - 1; yVelocity >= bottomRight.get(Y); yVelocity--) {
			for (int xVelocity = Math.min(topLeft.get(X), 0); xVelocity <= Math.max(bottomRight.get(X), 0); xVelocity++) {
				if (isTargetReachable(Math.abs(xVelocity), yVelocity)) {
					count++;
				}
			}
		}

		return count;
	}

	private boolean isTargetReachable(int xSpeed, int ySpeed) {
		int xHitPoint = 0;
		int yHitPoint = 0;
		while (yHitPoint >= bottomRight.get(Y) && xHitPoint <= MathUtils.maxOfAbs(topLeft.get(X), bottomRight.get(X))) {
			if (yHitPoint <= topLeft.get(Y) && xHitPoint >= MathUtils.minOfAbs(topLeft.get(X), bottomRight.get(X))) {
				return true;
			}
			yHitPoint += ySpeed--;
			xHitPoint += xSpeed;
			if (xSpeed > 0) {
				xSpeed--;
			}
		}
		return false;
	}
}
