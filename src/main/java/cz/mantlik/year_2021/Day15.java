package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day15 extends AbstractDay {

	private static final int MAP_DIMENSION = 5;

	private final List<Integer> risks = new ArrayList<>();
	private int rowSize;

	public Day15() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		risks.addAll(Arrays.stream(input.split("")).map(Integer::parseInt).collect(Collectors.toList()));
		rowSize = input.length();
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("The lowest total risk of any path from the top left to the bottom right is {0}.", dijkstra(risks.size() - 1));
	}

	@Override
	protected void setupSecondPart() {
		risks.clear();
	}

	private long dijkstra(int wantedNode) {
		long[] dijkstraArray = initDijkstraArray();
		Set<Integer> settled = new HashSet<>();
		Set<Integer> unsettled = new HashSet<>();

		unsettled.add(0);
		while (!unsettled.isEmpty() && !settled.contains(wantedNode)) {
			int currentNode = unsettled.stream().min(Comparator.comparingLong(u -> dijkstraArray[u])).orElse(-1);
			Set<Integer> unsettledNeighbors = getNeighbors(currentNode).stream().filter(n -> !settled.contains(n)).collect(Collectors.toSet());

			for (int neighborNode : unsettledNeighbors) {
				long newCost = dijkstraArray[currentNode] + risks.get(neighborNode);
				dijkstraArray[neighborNode] = Math.min(newCost, dijkstraArray[neighborNode]);
				unsettled.add(neighborNode);
			}

			unsettled.remove(currentNode);
			settled.add(currentNode);
		}

		return dijkstraArray[wantedNode];
	}

	private long[] initDijkstraArray() {
		int nodeCount = risks.size();
		long[] dijkstraArray = new long[nodeCount];
		dijkstraArray[0] = 0;
		for (int i = 1; i < nodeCount; i++) {
			dijkstraArray[i] = Long.MAX_VALUE;
		}
		return dijkstraArray;
	}

	private Set<Integer> getNeighbors(int index) {
		return Stream.of(index - 1, index + 1, index - rowSize, index + rowSize).filter(i -> isNeighbor(index, i)).collect(Collectors.toSet());
	}

	private boolean isNeighbor(int i, int j) {
		if (j < 0 || j >= risks.size()) {
			return false;
		}
		int rowI = i / rowSize;
		int rowJ = j / rowSize;
		int colI = i % rowSize;
		int colJ = j % rowSize;
		boolean neighborRow = Math.abs(rowI - rowJ) == 1;
		boolean neighborCol = Math.abs(colI - colJ) == 1;
		return (neighborCol && rowI == rowJ) || (neighborRow && colI == colJ);
	}

	@Override
	protected void processLineSecondPart(String input) {
		rowSize = input.length() * MAP_DIMENSION;
		List<Integer> inputSegment = Arrays.stream(input.split("")).map(Integer::parseInt).collect(Collectors.toList());
		risks.addAll(inputSegment);
		copyAndIncreaseRisks(inputSegment);
	}

	private void copyAndIncreaseRisks(List<Integer> prevSegment) {
		for (int i = 1; i < MAP_DIMENSION; i++) {
			prevSegment = prevSegment.stream().map(risk -> risk == 9 ? 1 : risk + 1).collect(Collectors.toList());
			risks.addAll(prevSegment);
		}
	}

	@Override
	protected void finishSecondPart() {
		copyAndIncreaseRisks(risks);
		finishFirstPart();
	}
}
