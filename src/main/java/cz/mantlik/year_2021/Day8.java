package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day8 extends AbstractDay {

	private final Map<String, Integer> digits = new HashMap<>();
	private int uniqueNumbersCount = 0;

	private final Map<String, Long> segmentCounts = new HashMap<>();
	private int finalSum = 0;

	public Day8() {
		super(false);
		digits.put("abcefg", 0);
		digits.put("cf", 1);
		digits.put("acdeg", 2);
		digits.put("acdfg", 3);
		digits.put("bcdf", 4);
		digits.put("abdfg", 5);
		digits.put("abdefg", 6);
		digits.put("acf", 7);
		digits.put("abcdefg", 8);
		digits.put("abcdfg", 9);

		segmentCounts.put("a", 8L);
		segmentCounts.put("b", 6L);
		segmentCounts.put("c", 8L);
		segmentCounts.put("d", 7L);
		segmentCounts.put("e", 4L);
		segmentCounts.put("f", 9L);
		segmentCounts.put("g", 7L);
	}

	@Override
	protected void processLineFirstPart(String input) {
		String fourDigitOutput = input.split(" \\| ")[1];
		List<Integer> uniqueNumbers = Arrays.asList(1, 4, 7, 8);
		List<Integer> uniqueSizes = digits.entrySet().stream()
				.filter(e -> uniqueNumbers.contains(e.getValue()))
				.map(e -> e.getKey().length())
				.collect(Collectors.toList());

		for (String digit : fourDigitOutput.split(" ")) {
			if (uniqueSizes.contains(digit.length())) {
				uniqueNumbersCount += 1;
			}
		}
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("There are {0} instances of digits that use a unique number of segments.", uniqueNumbersCount);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		String[] inputParts = input.split(" \\| ");

		Map<String, String> segmentMapping = new HashMap<>();
		for (char segment = 'a'; segment < 'h'; segment++) {
			String stringSegment = Character.toString(segment);
			segmentMapping.put(stringSegment, getTranslation(inputParts[0], stringSegment));
		}

		translateFourDigitOutput(inputParts[1], segmentMapping);
	}

	private String getTranslation(String patterns, final String segment) {
		long segmentCount = Arrays.stream(patterns.split("")).filter(segment::contains).count();
		List<String> mappingCandidates = segmentCounts.entrySet().stream()
				.filter(e -> e.getValue().equals(segmentCount)).map(Map.Entry::getKey).collect(Collectors.toList());

		boolean isC = segmentCount == segmentCounts.get("c") && getPatternByLength(patterns, 2).contains(segment);
		boolean isG = segmentCount == segmentCounts.get("g") && !getPatternByLength(patterns, 4).contains(segment);

		int candidateIndex = 0;
		if (isC || isG) {
			candidateIndex = 1;
		}

		return mappingCandidates.get(candidateIndex);
	}

	private String getPatternByLength(String patterns, int length) {
		return Arrays.stream(patterns.split(" ")).filter(p -> p.length() == length).findFirst().orElse("");
	}

	private void translateFourDigitOutput(String fourDigitOutput, Map<String, String> segmentMapping) {
		String[] fourDigits = fourDigitOutput.split(" ");
		for (int i = 0; i < fourDigits.length; i++) {
			finalSum += Math.pow(10, fourDigits.length - i - 1.0) * translateDigit(fourDigits[i], segmentMapping);
		}
	}

	private int translateDigit(String digit, Map<String, String> segmentMapping) {
		StringBuilder result = new StringBuilder();
		for (String segment : digit.split("")) {
			result.append(segmentMapping.get(segment));
		}
		String sortedResult = result.toString().chars().sorted()
				.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
		return digits.get(sortedResult);
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("Adding all of the output values produces {0}.", finalSum);
	}
}
