package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

public class Day2 extends AbstractDay {

	private int position = 0;
	private int depth = 0;
	private int aim = 0;

	public Day2() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		String command = input.split(" ")[0];
		int x = Integer.parseInt(input.split(" ")[1]);
		switch (command) {
			case "forward" :
				position += x;
				break;
			case "down":
				depth += x;
				break;
			case "up":
				depth -= x;
				break;
			default:
				break;
		}
	}

	@Override
	protected void finishFirstPart() {
		int product = position * depth;
		LOG.info("Multiplying horizontal position and depth together produces {0}.", product);
	}

	@Override
	protected void setupSecondPart() {
		depth = 0;
	}

	@Override
	protected void processLineSecondPart(String input) {
		String command = input.split(" ")[0];
		int x = Integer.parseInt(input.split(" ")[1]);
		switch (command) {
			case "forward" :
				depth += aim * x;
				break;
			case "down":
				aim += x;
				break;
			case "up":
				aim -= x;
				break;
			default:
				break;
		}
	}

	@Override
	protected void finishSecondPart() {
		int product = position * depth;
		LOG.info("Multiplying horizontal position and depth together produces {0}.", product);
	}
}
