package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.*;
import java.util.stream.Collectors;

public class Day11 extends AbstractDay {

	private final List<List<Octopus>> octopuses = new ArrayList<>();
	private long flashCount = 0;

	public Day11() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		octopuses.add(Arrays.stream(input.split("")).map(Octopus::new).collect(Collectors.toList()));
	}

	@Override
	protected void finishFirstPart() {
		int steps = 100;
		for (int i = 0; i < steps; i++) {
			step();
		}
		LOG.info("After {0} steps, there have been a total of {1}} flashes.", steps, flashCount);
	}

	@Override
	protected void setupSecondPart() {
		octopuses.clear();
	}

	private void step() {
		for (int y = 0; y < octopuses.size(); y++) {
			for (int x = 0; x < octopuses.get(y).size(); x++) {
				increaseEnergy(x, y);
			}
		}

		octopuses.stream().flatMap(Collection::stream).forEach(Octopus::calm);
	}

	private void increaseEnergy(int x, int y) {
		Octopus octopus = octopuses.get(y).get(x);
		octopus.energy += 1;
		if (octopus.flashed || octopus.energy <= 9) {
			return;
		}

		octopus.flashed = true;
		flashCount += 1;
		for (int i = x - 1; i < x + 2; i++) {
			for (int j = y - 1; j < y + 2; j++) {
				if (indexOutOfBounds(octopuses, j) || indexOutOfBounds(octopuses.get(j), i) || (i == x && j == y)) {
					continue;
				}
				increaseEnergy(i, j);
			}
		}
	}

	private <T> boolean indexOutOfBounds(List<T> list, int index) {
		return index < 0 || index >= list.size();
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLineFirstPart(input);
	}

	@Override
	protected void finishSecondPart() {
		for (long i = 0; i < Long.MAX_VALUE; i++) {
			step();
			if (octopuses.stream().flatMap(Collection::stream).allMatch(o -> o.energy == 0)) {
				LOG.info("The first time all octopuses flash simultaneously is step {0}.", i + 1);
				break;
			}
		}
	}

	private static class Octopus {

		private int energy;
		private boolean flashed = false;

		public Octopus(String energy) {
			this.energy = Integer.parseInt(energy);
		}

		public void calm() {
			if (energy > 9) {
				energy = 0;
				flashed = false;
			}
		}

		@Override
		public String toString() {
			return "" + energy;
		}
	}
}
