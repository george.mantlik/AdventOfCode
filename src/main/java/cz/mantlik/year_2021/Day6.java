package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;

import java.util.*;
import java.util.stream.Collectors;

public class Day6 extends AbstractDay {

	private final List<Integer> fish = new ArrayList<>();

	private final Map<Integer, Long> fishByDay = new HashMap<>();

	public Day6() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		fish.addAll(Arrays.stream(input.split(",")).map(Integer::parseInt).collect(Collectors.toList()));
	}

	@Override
	protected void finishFirstPart() {
		for (int i = 0; i < 80; i++) {
			newDay();
		}
		LOG.info("After 80 days, there would be a total of {0} fish.", fish.size());
	}

	@Override
	protected void setupSecondPart() {
		fish.clear();
	}

	private void newDay() {
		List<Integer> toAdd = new ArrayList<>();
		for (int i = 0; i < fish.size(); i++) {
			int singleFish = fish.get(i);
			if (singleFish > 0) {
				singleFish -= 1;
			} else {
				singleFish = 6;
				toAdd.add(8);
			}
			fish.set(i, singleFish);
		}
		fish.addAll(toAdd);
	}

	@Override
	protected void processLineSecondPart(String input) {
		fish.addAll(Arrays.stream(input.split(",")).map(Integer::parseInt).collect(Collectors.toList()));
		for (Integer singleFish : fish) {
			long fishThisDay = fishByDay.getOrDefault(singleFish, 0L);
			fishByDay.put(singleFish, fishThisDay + 1);
		}
	}

	@Override
	protected void finishSecondPart() {
		int days = 256;
		for (int today = 0; today < days; today++) {
			int nextFertileDay = today + 7;
			int newbornFertileDay = today + 9;
			long fishToday = fishByDay.getOrDefault(today, 0L);
			fishByDay.put(newbornFertileDay, fishToday);
			fishByDay.put(nextFertileDay, fishToday + fishByDay.getOrDefault(nextFertileDay, 0L));
			fishByDay.remove(today);
		}

		long count = fishByDay.values().stream().reduce(Long::sum).orElse(0L);
		LOG.info("After {0} days, there would be a total of {1} fish.", days, count);
	}
}
