package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class Day12 extends AbstractDay {

	private static final String START = "start";
	private static final String END = "end";

	private final Map<String, Cave> caves = new HashMap<>();

	private BiPredicate<Cave, List<String>> wrongWayCondition;

	public Day12() {
		super(false);
		wrongWayCondition = (cave, path) -> true;
	}

	@Override
	protected void processLineFirstPart(String input) {
		String[] caveNames = input.split("-");
		Cave cave1 = fetchCave(caveNames[0]);
		Cave cave2 = fetchCave(caveNames[1]);
		cave1.neighbors.add(cave2);
		cave2.neighbors.add(cave1);
	}

	private Cave fetchCave(String name) {
		if (!caves.containsKey(name)) {
			caves.put(name, new Cave(name));
		}
		return caves.get(name);
	}

	@Override
	protected void finishFirstPart() {
		int pathCount = countPaths(caves.get(START), new ArrayList<>());
		LOG.info("There are {0} paths through this cave system.", pathCount);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	private int countPaths(Cave cave, List<String> path) {
		if (cave.name.equals(END)) {
			return 1;
		}
		if (cave.isSmall() && path.contains(cave.name) && wrongWayCondition.test(cave, path)) {
			return 0;
		}
		path.add(cave.name);
		int pathCount = 0;
		for (Cave neighbor : cave.neighbors) {
			if (neighbor.name.equals(START)) {
				continue;
			}
			pathCount += countPaths(neighbor, new ArrayList<>(path));
		}
		return pathCount;
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Nothing
	}

	@Override
	protected void finishSecondPart() {
		wrongWayCondition = (cave, path) -> {
			Predicate<String> duplicateFilter = c -> path.stream().filter(x -> x.equals(c)).count() > 1;
			return path.stream().filter(StringUtils::isAllLowerCase).anyMatch(duplicateFilter);
		};
		finishFirstPart();
	}

	private static class Cave {

		private final String name;
		private final List<Cave> neighbors = new ArrayList<>();

		public Cave(String name) {
			this.name = name;
		}

		public boolean isSmall() {
			return StringUtils.isAllLowerCase(name);
		}
	}
}
