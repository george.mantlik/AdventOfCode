package cz.mantlik.year_2021;

import cz.mantlik.common.AbstractDay;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Day20 extends AbstractDay {

	private final List<Character> enhancementAlgorithm = new ArrayList<>();
	private final List<Character> image = new ArrayList<>();
	private int imageWidth;

	public Day20() {
		super(true);
	}

	@Override
	protected void processLineFirstPart(String input) {
		if (enhancementAlgorithm.isEmpty()) {
			enhancementAlgorithm.addAll(convertInput(input));
		} else if (!input.isEmpty()) {
			input = StringUtils.leftPad(input, input.length() + 2, ".");
			input = StringUtils.rightPad(input, input.length() + 2, ".");
			if (image.isEmpty()) {
				imageWidth = input.length();
				createEmptyRows();
			}
			image.addAll(convertInput(input));
		}
	}

	private List<Character> convertInput(String input) {
		return Arrays.stream(input.split("")).map(this::convertPixel).collect(Collectors.toList());
	}

	private char convertPixel(String input) {
		return input.equals("#") ? '1' : '0';
	}

	private void createEmptyRows() {
		image.addAll(Collections.nCopies(2 * imageWidth, '0'));
	}

	@Override
	protected void finishFirstPart() {
		createEmptyRows();
		enhance();
		enhance();
		long litPixels = image.stream().filter(p -> p == '1').count();
		LOG.info("{0} pixels are lit in the resulting image.", litPixels);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	private void enhance() {
		List<Character> newImage = new ArrayList<>();
		for (int i = 0; i < image.size(); i++) {
			newImage.add(enhancePixel(i));
		}
		image.clear();
		image.addAll(newImage);
	}

	private char enhancePixel(int pixelIndex) {
		StringBuilder algorithmIndexBuilder = new StringBuilder();
		for (int y = -1; y < 2; y++) {
			for (int x = -1; x < 2; x++) {
				int neighborIndex = pixelIndex + x + y * imageWidth;
				algorithmIndexBuilder.append(neighborValid(pixelIndex, neighborIndex) ? image.get(neighborIndex) : image.get((pixelIndex)));
			}
		}
		int algorithmIndex = Integer.parseInt(algorithmIndexBuilder.toString(), 2);
		return enhancementAlgorithm.get(algorithmIndex);
	}

	private boolean neighborValid(int origin, int neighbor) {
		int originCol = origin % imageWidth;
		int neighborCol = neighbor % imageWidth;
		return neighbor >= 0 && neighbor < image.size() && Math.abs(originCol - neighborCol) <= 1;
	}

	@Override
	protected void processLineSecondPart(String input) {

	}

	@Override
	protected void finishSecondPart() {

	}
}
