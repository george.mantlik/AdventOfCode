package cz.mantlik.year_2021;

import cz.mantlik.puzzleutils.Point;
import cz.mantlik.puzzleutils.PuzzleLogger;
import cz.mantlik.common.AbstractDay;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.*;

public class Day13 extends AbstractDay {

	private final Set<Point> dotPositions = new HashSet<>();
	private int foldCounter = 0;

	public Day13() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		processLine(input, c -> c >= 1);
	}

	private void processLine(String input, IntPredicate foldStopper) {
		if (input.startsWith("fold along ")) {
			String[] instructions = input.replace("fold along ", "").split("=");
			if (!foldStopper.test(foldCounter)) {
				fold(instructions[0], Integer.parseInt(instructions[1]));
			}
			foldCounter += 1;
		} else if (!input.isEmpty()) {
			Integer[] coordinates = Arrays.stream(input.split(",")).map(Integer::parseInt).toArray(Integer[]::new);
			dotPositions.add(new Point(coordinates[0], coordinates[1]));
		}
	}

	private void fold(String axis, int line) {
		int axisIndex = axis.equals("x") ? Point.X : Point.Y;
		Set<Point> newPositions = new HashSet<>();
		for (Point position : dotPositions) {
			int newCoordinate = 2 * line - position.get(axisIndex);
			if (newCoordinate < line) {
				position.set(axisIndex, newCoordinate);
			}
			newPositions.add(position);
		}
		dotPositions.clear();
		dotPositions.addAll(newPositions);
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("After the first fold, {0} dots are visible", dotPositions.size());
	}

	@Override
	protected void setupSecondPart() {
		dotPositions.clear();
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLine(input, c -> false);
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("The code to activate the infrared thermal imaging camera system is:");

		int biggestX = getBiggestCoord(Point.X);
		int biggestY = getBiggestCoord(Point.Y);

		for (int y = 0; y <= biggestY; y++) {
			StringBuilder lineBuilder = new StringBuilder();
			for (int x = 0; x <= biggestX; x++) {
				lineBuilder.append(dotPositions.contains(new Point(x, y)) ? PuzzleLogger.DRAW_LINE : PuzzleLogger.DRAW_SPACE);
			}
			String line = lineBuilder.toString();
			if (!line.trim().isEmpty()) {
				LOG.info("{0}", lineBuilder);
			}
		}
	}

	private int getBiggestCoord(int axisIndex) {
		return dotPositions.stream().map(p -> p.get(axisIndex)).sorted().reduce((first, second) -> second).orElse(0);
	}
}
