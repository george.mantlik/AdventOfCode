package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day9 extends AbstractDayNg {

	public Day9() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private long eValuesSum = 0;

		@Override
		public void processLine(String input) {
			List<Long> oasisValueHistory = Arrays.stream(input.split(" ")).map(Long::parseLong).collect(Collectors.toList());
			eValuesSum += countNextValue(oasisValueHistory);
		}

		protected long countNextValue(List<Long> valueHistory) {
			if (valueHistory.stream().allMatch(v -> v == 0)) {
				return 0;
			}

			List<Long> differences = countDifferences(valueHistory);
			return valueHistory.get(valueHistory.size() - 1) + countNextValue(differences);
		}

		protected List<Long> countDifferences(List<Long> values) {
			List<Long> differences = new ArrayList<>();
			for (int i = 0; i < values.size() - 1; i++) {
				differences.add(values.get(i + 1) - values.get(i));
			}
			return differences;
		}

		@Override
		public Object finish() {
			LOG.info("Sum of extrapolated next values for each history is {0}", eValuesSum);
			return eValuesSum;
		}

	}

	private static class SecondPart extends FirstPart {

		@Override
		protected long countNextValue(List<Long> valueHistory) {
			if (valueHistory.stream().allMatch(v -> v == 0)) {
				return 0;
			}

			List<Long> differences = countDifferences(valueHistory);
			return valueHistory.get(0) - countNextValue(differences);
		}
	}
}
