package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.HashMap;
import java.util.Map;

public class Day1 extends AbstractDayNg {

	private static final Map<String, String> DIGITS;

	static {
		DIGITS = new HashMap<>();
		DIGITS.put("one", "1");
		DIGITS.put("two", "2");
		DIGITS.put("three", "3");
		DIGITS.put("four", "4");
		DIGITS.put("five", "5");
		DIGITS.put("six", "6");
		DIGITS.put("seven", "7");
		DIGITS.put("eight", "8");
		DIGITS.put("nine", "9");
	}

	public Day1() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private int calibrationSum = 0;

		@Override
		public void processLine(String input) {
			String digits = input.replaceAll("\\D", "");
			if (!digits.isEmpty()) {
				String calibrationValue = "" + digits.charAt(0) + digits.charAt(digits.length() - 1);
				calibrationSum += Integer.parseInt(calibrationValue);
			}
		}

		@Override
		public Object finish() {
			LOG.info("Sum of all of the calibration values is {0}.", calibrationSum);
			return calibrationSum;
		}
	}

	private static class SecondPart extends FirstPart {

		@Override
		public void processLine(String input) {
			for (Map.Entry<String, String> digit : DIGITS.entrySet()) {
				String digitName = digit.getKey();
				String replacement = digitName.charAt(0) + digit.getValue() + digitName.charAt(digitName.length() - 1);
				input = input.replaceAll(digitName, replacement);
			}

			super.processLine(input);
		}
	}
}
