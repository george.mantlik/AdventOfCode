package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class Day4 extends AbstractDayNg {

	public Day4() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private long totalPoints = 0;

		@Override
		public void processLine(String input) {
			long wonCount = getWonCount(input);
			if (wonCount > 0) {
				totalPoints += 1L << wonCount - 1;
			}
		}

		@Override
		public Object finish() {
			LOG.info("The scratchcards are worth {0} points in total.", totalPoints);
			return totalPoints;
		}

		protected List<String> getNumbers(String input) {
			return Arrays.asList(input.split(" "));
		}

		protected long getWonCount(String input) {
			String[] parts = input.split(": | \\| ");
			List<String> winning = getNumbers(parts[1]);
			return getNumbers(parts[2]).stream().filter(StringUtils::isNotEmpty).filter(winning::contains).count();
		}

	}

	private static class SecondPart extends FirstPart {

		private final List<Long> cardAmounts = new ArrayList<>();
		private final List<Long> scores = new ArrayList<>();

		@Override
		public void processLine(String input) {
			long wonCount = getWonCount(input);
			cardAmounts.add(1L);
			scores.add(wonCount);
		}

		@Override
		public Object finish() {
			for (int i = 0; i < cardAmounts.size(); i++) {
				for (int j = i + 1; j <= i + scores.get(i); j++) {
					long newAmount = cardAmounts.get(j) + cardAmounts.get(i);
					cardAmounts.set(j, newAmount);
				}
			}
			long totalCards = cardAmounts.stream().reduce(Long::sum).orElse(0L);
			LOG.info("We end up with {0} scratchcard.", totalCards);
			return totalCards;
		}
	}
}
