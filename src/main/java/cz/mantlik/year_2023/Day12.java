package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day12 extends AbstractDayNg {

	public Day12() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private String springs;
		private List<Integer> checkSums;
		private long arrangementsCount = 0;

		@Override
		public void processLine(String input) {
			String[] inputParts = input.split(" ");
			springs = inputParts[0];
			checkSums = Arrays.stream(inputParts[1].split(",")).map(Integer::parseInt).collect(Collectors.toList());
			arrangementsCount += countMatchingArrangements("");
//			boolean matches = springArrangementsMatchControlSum(springs);
//			LOG.info("Does it match the checksum? {0}", matches);
//			LOG.info("Partial sum: {0}", arrangementsCount);
		}

		private long countMatchingArrangements(String springsArrangement) {
			if (springsArrangement.length() >= springs.length()) {
				if (!springsArrangementMatchesControlSum(springsArrangement)) {
//					LOG.info("Discarding: {0}", springsArrangement);
					return 0;
				}
//				LOG.info("Ok: {0}", springsArrangement);
				return 1;
			}

			char currentSpring = springs.charAt(springsArrangement.length());
			return checkAndCount(springsArrangement, currentSpring, '.') + checkAndCount(springsArrangement, currentSpring, '#');
		}

		private long checkAndCount(String springsArrangement, char currentSpring, char option) {
			if (currentSpring == '?' || currentSpring == option) {
				return countMatchingArrangements(springsArrangement + option);
			}
			return 0;
		}

		private boolean springsArrangementMatchesControlSum(String springsArrangement) {
			String[] brokenSprings = springsArrangement.replaceFirst("^\\.+", "").split("\\.+");
			if (brokenSprings.length != checkSums.size()) {
				return false;
			}
//			LOG.info("Broken: {0}", Arrays.asList(brokenSprings));
			for (int i = 0; i < brokenSprings.length; i++) {
				if (brokenSprings[i].length() != checkSums.get(i)) {
					return false;
				}
			}
			return true;
		}

		@Override
		public Object finish() {
			LOG.info("The sum of counts of all different arrangements of operational and broken springs is {0}.", arrangementsCount);
			return arrangementsCount;
		}

	}

	private static class SecondPart extends FirstPart {
	}
}
