package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.*;
import java.util.stream.LongStream;

public class Day6 extends AbstractDayNg {

	public Day6() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private final List<Long> times = new ArrayList<>();
		private final List<Long> distances = new ArrayList<>();

		@Override
		public void processLine(String input) {
			String[] inputParts = input.split(" +");
			List<Long> listToFill = inputParts[0].startsWith("Time") ? times : distances;

			for (int i = 1; i < inputParts.length; i++) {
				listToFill.add(Long.parseLong(inputParts[i]));
			}
		}

		@Override
		public Object finish() {
			long result = 1L;
			for (int i = 0; i < times.size(); i++) {
				result *= countWinningOptions(times.get(i), distances.get(i));
			}
			logResult(result);
			return result;
		}

		private long countWinningOptions(long time, long recordDistance) {
			return LongStream.range(0, time).map(t -> t * (time - t)).filter(d -> d > recordDistance).count();
		}

		protected void logResult(long result) {
			LOG.info("The result of multiplying together the numbers of ways to beat the records is {0}.", result);
		}

	}

	private static class SecondPart extends FirstPart {

		@Override
		public void processLine(String input) {
			super.processLine(input.replace(" ", "").replaceFirst(":", " "));
		}

		@Override
		protected void logResult(long result) {
			LOG.info("In this much longer race there is {0} ways to beat the record.", result);
		}
	}
}
