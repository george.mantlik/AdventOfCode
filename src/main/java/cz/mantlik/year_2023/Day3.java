package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;
import cz.mantlik.puzzleutils.Point;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day3 extends AbstractDayNg {

	public Day3() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		protected int currentLine = 0;
		protected final Map<Point, Integer> pointValues = new HashMap<>();
		protected final List<Point> symbolPositions = new ArrayList<>();

		@Override
		public void processLine(String input) {
			StringBuilder numberBuilder = new StringBuilder();

			for (int i = 0; i < input.length(); i++) {
				char character = input.charAt(i);
				if (Character.isDigit(character)) {
					numberBuilder.append(character);

					if (i == input.length() - 1 || !Character.isDigit(input.charAt(i + 1))) {
						savePartNumber(i, numberBuilder);
					}
				} else if (isWantedSymbol(character)) {
					symbolPositions.add(new Point(i, currentLine));
				}
			}
			currentLine++;
		}

		@Override
		public Object finish() {
			int sumOfPartNumbers = symbolPositions.stream().map(pointValues::get).reduce(Integer::sum).orElse(0);
			LOG.info("Sum of all of the part numbers in the engine schematic is {0}.", sumOfPartNumbers);
			return sumOfPartNumbers;
		}

		private void savePartNumber(int end, StringBuilder partNumberBuilder) {
			int partNumber = Integer.parseInt(partNumberBuilder.toString());
			int numberLength = partNumberBuilder.length();
			partNumberBuilder.delete(0, numberLength);
			for (int y = currentLine - 1; y <= currentLine + 1; y++) {
				for (int x = end - numberLength; x <= end + 1; x++) {
					Point point = new Point(x, y);
					savePartNumberOnPoint(partNumber, point);
				}
			}
		}

		protected void savePartNumberOnPoint(int partNumber, Point point) {
			int pointSum = pointValues.computeIfAbsent(point, p -> 0);
			pointValues.put(point, pointSum + partNumber);
		}

		protected boolean isWantedSymbol(char character) {
			return character != '.';
		}

	}

	private static class SecondPart extends FirstPart {

		private final Map<Point, Integer> singleValuePoints = new HashMap<>();

		@Override
		public Object finish() {
			int sumOfGearRatios = symbolPositions.stream()
					.filter(pointValues::containsKey)
					.map(pointValues::get)
					.reduce(Integer::sum).orElse(0);
			LOG.info("Sum of all of all of the gear ratios in the engine schematic is {0}.", sumOfGearRatios);
			return sumOfGearRatios;
		}

		@Override
		protected void savePartNumberOnPoint(int partNumber, Point point) {
			if (singleValuePoints.containsKey(point)) {
				pointValues.put(point, singleValuePoints.get(point) * partNumber);
			} else {
				singleValuePoints.put(point, partNumber);
			}
		}

		@Override
		protected boolean isWantedSymbol(char character) {
			return character == '*';
		}
	}

}
