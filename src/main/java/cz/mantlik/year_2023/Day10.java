package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;
import cz.mantlik.puzzleutils.Point;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day10 extends AbstractDayNg {

	public Day10() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		protected final List<String> tiles = new ArrayList<>();
		private Point startPosition;

		@Override
		public void processLine(String input) {
			int startIndex = input.indexOf("S");
			if (startIndex >= 0) {
				startPosition = new Point(startIndex, tiles.size());
			}
			tiles.add(input);
		}

		@Override
		public Object finish() {
			int stepsToFarthestPoint = countStepsToFarthestPoint(findFollowingPipe());
			LOG.info("It takes {0} steps along the loop to get from the starting position to the point farthest from the starting position", stepsToFarthestPoint);
			return stepsToFarthestPoint;
		}

		private Point findFollowingPipe() {
			List<Point> startNeighbors = startPosition.getNeighbors();
			for (Point neighbor : startNeighbors) {
				if (countNextPoint(neighbor, startPosition) != null) {
					return neighbor;
				}
			}
			return null;
		}

		private int countStepsToFarthestPoint(Point currentPosition) {
			int counter = 0;
			Point previousPosition = startPosition;

			while (!startPosition.equals(currentPosition)) {
				Point nextPosition = countNextPoint(currentPosition, previousPosition);
				if (nextPosition == null) {
					break;
				}
				previousPosition = currentPosition;
				currentPosition = nextPosition;
				counter++;
			}

			return (counter + 1) / 2;
		}

		protected Point countNextPoint(Point current, Point previous) {
			if (!current.isInBounds(new Point(0, 0), new Point(tiles.get(0).length(), tiles.size()))) {
				return null;
			}
			Optional<Pipe> pipe = Pipe.fromChar(tiles.get(current.get(Point.Y)).charAt(current.get(Point.X)));

			List<Point> connectingTilePositions = pipe.map(p -> p.getPointsConnectedTo(current)).orElse(Collections.emptyList());
			if (!connectingTilePositions.contains(previous)) {
				return null;
			}

			connectingTilePositions.remove(previous);
			return connectingTilePositions.get(0);
		}

	}

	private static class SecondPart extends FirstPart {

	}

	private enum Pipe {
		NS('|'),
		EW('-'),
		NE('L'),
		NW('J'),
		SE('F'),
		SW('7');

		private final char symbol;
		private final List<Point> pointModifiers;

		Pipe(char symbol) {
			this.symbol = symbol;
			this.pointModifiers = IntStream.range(0, 2)
					.mapToObj(i -> Direction.getPointModifier(name().substring(i, i + 1)))
					.collect(Collectors.toList());
		}

		public List<Point> getPointsConnectedTo(Point point) {
			return pointModifiers.stream()
					.map(m -> {
						Point p = new Point(m);
						p.add(point);
						return p;
					})
					.collect(Collectors.toList());
		}

		public static Optional<Pipe> fromChar(char symbol) {
			return Arrays.stream(values()).filter(v -> v.symbol == symbol).findFirst();
		}
	}

	private enum Direction {
		N(0, -1),
		S(0, 1),
		E(1, 0),
		W(-1, 0);

		private final Point pointModifier;

		Direction(int xModifier, int yModifier) {
			this.pointModifier = new Point(xModifier, yModifier);
		}

		public static Point getPointModifier(String directionName) {
			return valueOf(directionName).pointModifier;
		}
	}
}
