package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;
import cz.mantlik.puzzleutils.Point;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day11 extends AbstractDayNg {

	public Day11() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private final List<Point> galaxies = new ArrayList<>();
		private final Set<Integer> emptyRows = new HashSet<>();
		private final Set<Integer> emptyCols = new HashSet<>();

		private int rowCounter = 0;

		@Override
		public void processLine(String input) {
			if (!input.contains("#")) {
				emptyRows.add(rowCounter);
			}
			for (int i = 0; i < input.length(); i++) {
				char pixel = input.charAt(i);
				if (pixel == '#') {
					galaxies.add(new Point(i, rowCounter));
					emptyCols.remove(i);
				} else if (rowCounter == 0) {
					emptyCols.add(i);
				}
			}
			rowCounter++;
		}

		@Override
		public Object finish() {
			moveGalaxies();

			long distanceSum = 0;
			for (int i = 0; i < galaxies.size(); i++) {
				for (int j = i + 1; j < galaxies.size(); j++) {
					distanceSum += galaxies.get(i).manhattanDistance(galaxies.get(j));
				}
			}

			LOG.info("The sum of the lengths of the shortest path between every pair of galaxies after the expansion is {0}.", distanceSum);
			return distanceSum;
		}

		private void moveGalaxies() {
			for (Point galaxy : galaxies) {
				int xExpansion = getExpansionSize(emptyCols, galaxy.get(Point.X));
				int yExpansion = getExpansionSize(emptyRows, galaxy.get(Point.Y));
				galaxy.add(new Point(xExpansion, yExpansion));
			}
		}

		protected int getExpansionSize(Set<Integer> emptySpaces, int expandingPoint) {
			return (int) emptySpaces.stream().filter(s -> s < expandingPoint).count();
		}

	}

	private static class SecondPart extends FirstPart {
		@Override
		protected int getExpansionSize(Set<Integer> emptySpaces, int expandingPoint) {
			return super.getExpansionSize(emptySpaces, expandingPoint) * 999999;
		}
	}
}
