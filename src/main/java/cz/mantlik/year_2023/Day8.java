package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;
import org.apache.commons.lang3.StringUtils;
import cz.mantlik.puzzleutils.MathUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class Day8 extends AbstractDayNg {

	public Day8() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private String directions;
		protected final Map<String, Node> nodes = new HashMap<>();

		@Override
		public void processLine(String input) {
			if (StringUtils.isEmpty(directions)) {
				directions = input;
			} else if (StringUtils.isNotEmpty(input)) {
				String[] nodeNames = input.split(" = \\(|, |\\)");
				nodes.put(nodeNames[0], new Node(nodeNames[1], nodeNames[2]));
			}
		}

		@Override
		public Object finish() {
			long steps = countSteps("AAA", "ZZZ"::equals);
			LOG.info("It takes {0} steps to reach ZZZ.", steps);
			return steps;
		}

		protected long countSteps(String startNode, Predicate<String> endNodeCondition) {
			long step = 0;
			String currentNode = startNode;
			while (!endNodeCondition.test(currentNode)) {
				currentNode = moveNode(currentNode, step++);
			}
			return step;
		}

		protected String moveNode(String node, long step) {
			return nodes.get(node).getChild(directions.charAt((int) (step % directions.length())));
		}

	}

	private static class SecondPart extends FirstPart {

		@Override
		public Object finish() {
//			int step = 0;
//			List<String> currentNodes = nodes.keySet().stream()
//					.filter(n -> n.endsWith("A"))
//					.collect(Collectors.toList());
			Predicate<String> endCondition = n -> n.endsWith("Z");
			long steps = nodes.keySet().stream()
					.filter(n -> n.endsWith("A"))
					.map(n -> countSteps(n, endCondition))
					.reduce(MathUtils::lcm)
					.orElse(0L);
//			while (!currentNodes.stream().allMatch(n -> n.endsWith("Z"))) {
//				currentNodes = moveNodes(currentNodes, step++);
//			}
			LOG.info("It takes {0} steps to be only on nodes that end with Z.", steps);
			return steps;
		}

		private List<String> moveNodes(List<String> currentNodes, int step) {
			List<String> newNodes = new ArrayList<>();
			for (String currentNode : currentNodes) {
				newNodes.add(moveNode(currentNode, step));
			}
			return newNodes;
		}
	}

	private static class Node {

		private final String left;
		private final String right;

		private Node(String left, String right) {
			this.left = left;
			this.right = right;
		}

		private String getChild(char direction) {
			return direction == 'L' ? left : right;
		}
	}
}
