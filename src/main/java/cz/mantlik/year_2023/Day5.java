package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.*;

public class Day5 extends AbstractDayNg {

	public Day5() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private final List<Resource> resources = new ArrayList<>();

		@Override
		public void processLine(String input) {
			if (input.isEmpty()) {
				getResources().forEach(Resource::nextRound);
			} else if (input.startsWith("seeds:")) {
				initSeeds(input.split(" "));
			} else if (!input.contains(":")) {
				String[] mapping = input.split(" ");
				long dstStart = parse(mapping[0]);
				long srcStart = parse(mapping[1]);
				long srcEnd = srcStart + parse(mapping[2]) - 1;
				processMapping(dstStart, srcStart, srcEnd);
			}
		}

		@Override
		public Object finish() {
			Long lowestLocNumber = getResources().stream()
					.map(s -> s.number)
					.reduce(Long::min)
					.orElseThrow(()-> new RuntimeException("No location number found."));
			LOG.info("The lowest location number that corresponds to any of the initial seed numbers is {0}.", lowestLocNumber);
			return lowestLocNumber;
		}

		protected Collection<Resource> getResources() {
			return resources;
		}

		protected void initSeeds(String[] seeds) {
			for (int i = 1; i < seeds.length; i++) {
				resources.add(new Resource(parse(seeds[i])));
			}
		}

		protected void processMapping(long dstStart, long srcStart, long srcEnd) {
			for (Resource resource : resources) {
				if (resource.processed || resource.number < srcStart || resource.number > srcEnd) {
					continue;
				}
				resource.updateNumber(dstStart - srcStart);
			}
		}

		protected Long parse(String stringValue) {
			return Long.parseLong(stringValue);
		}

	}

	private static class SecondPart extends FirstPart {

		private final Map<Resource, Long> resourceRanges = new HashMap<>();

		@Override
		protected Collection<Resource> getResources() {
			return resourceRanges.keySet();
		}

		@Override
		protected void initSeeds(String[] seeds) {
			for (int i = 1; i < seeds.length - 1; i += 2) {
				resourceRanges.put(new Resource(parse(seeds[i])), parse(seeds[i + 1]));
			}
		}

		@Override
		protected void processMapping(long dstStart, long srcStart, long srcEnd) {
			Map<Resource, Long> newRanges = new HashMap<>();
			Set<Resource> modifiedResources = new HashSet<>();
			for (Map.Entry<Resource, Long> resourceRange : resourceRanges.entrySet()) {
				Resource resource = resourceRange.getKey();
				long rangeEnd = resource.number + resourceRange.getValue() - 1;
				if (resource.processed || rangeEnd < srcStart || resource.number > srcEnd) {
					continue;
				}
				modifiedResources.add(resource);

				long crossStart = Math.max(resource.number, srcStart);
				long crossEnd = Math.min(rangeEnd, srcEnd);
				if (resource.number < srcStart) {
					newRanges.put(resource, srcStart - resource.number);
				}
				if (rangeEnd > srcEnd) {
					newRanges.put(new Resource(srcEnd + 1), rangeEnd - srcEnd);
				}
				Resource mappedResource = new Resource(crossStart);
				mappedResource.updateNumber(dstStart - srcStart);
				newRanges.put(mappedResource, crossEnd - crossStart);
			}

			resourceRanges.keySet().removeAll(modifiedResources);
			resourceRanges.putAll(newRanges);
		}
	}

	private static class Resource {

		private Long number;
		private boolean processed = false;

		public Resource(Long number) {
			this.number = number;
		}

		public void updateNumber(Long modifier) {
			number += modifier;
			processed = true;
		}

		public void nextRound() {
			processed = false;
		}

	}

}
