package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.*;
import java.util.stream.Collectors;

public class Day7 extends AbstractDayNg {

	private static final List<Character> CARDS = Arrays.asList('2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A');

	public Day7() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private final List<Hand> hands = new ArrayList<>();
		protected final Map<Integer, Integer> currentCardCounts = new HashMap<>();

		@Override
		public void processLine(String input) {
			String[] handInfo = input.split(" ");
			hands.add(new Hand(handInfo, getHandType(handInfo[0])));
		}

		@Override
		public Object finish() {
			List<Hand> sortedHands = hands.stream()
					.sorted(this::comparingHands)
					.collect(Collectors.toList());
			long totalWinnings = 0L;
			for (int i = 0; i < sortedHands.size(); i++) {
				totalWinnings += (i + 1L) * sortedHands.get(i).bid;
			}
			LOG.info("The total winnings of this set of hands are {0}.", totalWinnings);
			return totalWinnings;
		}

		private int comparingHands(Hand h1, Hand h2) {
			int comparison = h1.type.compareTo(h2.type);

			int i = 0;
			while (comparison == 0 && i < 5) {
				comparison = getCardRank(h1, i).compareTo(getCardRank(h2, i));
				i++;
			}
			return comparison;
		}

		protected Integer getCardRank(Hand hand, int cardIndex) {
			return CARDS.indexOf(hand.cards.charAt(cardIndex));
		}

		private HandType getHandType(String cards) {
			currentCardCounts.clear();
			fillCurrentCardCounts(cards);

			switch (currentCardCounts.size()) {
				case 1:
					return HandType.FIVE;
				case 2:
					return currentCardCounts.containsValue(4) ? HandType.FOUR : HandType.FULL_HOUSE;
				case 3:
					return currentCardCounts.containsValue(3) ? HandType.THREE : HandType.TWO_PAIR;
				case 4:
					return HandType.ONE_PAIR;
				default:
					return HandType.HIGH_CARD;
			}
		}

		protected void fillCurrentCardCounts(String cards) {
			cards.chars().forEach(card -> currentCardCounts.merge(card, 1, Integer::sum));
		}

	}

	private static class SecondPart extends FirstPart {

		@Override
		protected Integer getCardRank(Hand hand, int cardIndex) {
			if (hand.cards.charAt(cardIndex) == 'J') {
				return -1;
			}
			return super.getCardRank(hand, cardIndex);
		}

		@Override
		protected void fillCurrentCardCounts(String cards) {
			super.fillCurrentCardCounts(cards);
			int joker = 'J';
			if (currentCardCounts.containsKey(joker) && currentCardCounts.size() > 1) {
				int jokerCount = currentCardCounts.remove(joker);
				currentCardCounts.keySet().forEach(card -> currentCardCounts.merge(card, jokerCount, Integer::sum));
			}
		}
	}

	private static class Hand {

		private final HandType type;
		private final String cards;
		private final int bid;

		public Hand(String[] handInfo, HandType type) {
			this.bid = Integer.parseInt(handInfo[1]);
			this.cards = handInfo[0];
			this.type = type;
		}

		@Override
		public String toString() {
			return cards + " (" + type + "), bids " + bid;
		}
	}

	private enum HandType {

		HIGH_CARD, ONE_PAIR, TWO_PAIR, THREE, FULL_HOUSE, FOUR, FIVE

	}

}
