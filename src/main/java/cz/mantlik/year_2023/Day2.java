package cz.mantlik.year_2023;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.HashMap;
import java.util.Map;

public class Day2 extends AbstractDayNg {

	public Day2() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		protected final Map<String, Integer> cubeAmounts = new HashMap<>();
		private int sumOfIDs = 0;

		public FirstPart() {
			cubeAmounts.put("red", 12);
			cubeAmounts.put("green", 13);
			cubeAmounts.put("blue", 14);
		}

		@Override
		public void processLine(String input) {
			String[] inputParts = getInputParts(input);
			int gameID = Integer.parseInt(inputParts[0]);

			for (int i = 1; i < inputParts.length; i++) {
				String[] cube = inputParts[i].split(" ");
				if (Integer.parseInt(cube[0]) > cubeAmounts.get(cube[1])) {
					return;
				}
			}

			sumOfIDs += gameID;
		}

		@Override
		public Object finish() {
			LOG.info("Sum of the IDs of the games that would have been possible is {0}.", sumOfIDs);
			return sumOfIDs;
		}

		protected String[] getInputParts(String input) {
			return input.replace("Game ", "").split(": |; |, ");
		}
	}

	private static class SecondPart extends FirstPart {

		private int powerSum = 0;

		@Override
		public void processLine(String input) {
			resetCubeAmounts();
			String[] inputParts = getInputParts(input);

			for (int i = 1; i < inputParts.length; i++) {
				String[] cube = inputParts[i].split(" ");
				int cubeCount = Integer.parseInt(cube[0]);
				if (cubeCount > cubeAmounts.get(cube[1])) {
					cubeAmounts.put(cube[1], cubeCount);
				}
			}

			powerSum += cubeAmounts.values().stream().reduce(1, (a, b) -> a * b);
		}

		@Override
		public Object finish() {
			LOG.info("Sum of the power of the minimum set of cubes that must have been present in each game is {0}.", powerSum);
			return powerSum;
		}

		private void resetCubeAmounts() {
			cubeAmounts.put("red", 0);
			cubeAmounts.put("green", 0);
			cubeAmounts.put("blue", 0);
		}
	}
}
