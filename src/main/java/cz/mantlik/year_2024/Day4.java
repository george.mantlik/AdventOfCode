package cz.mantlik.year_2024;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;
import cz.mantlik.puzzleutils.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day4 extends AbstractDayNg {

	public Day4() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		protected final List<String> wordSearch = new ArrayList<>();

		protected String wantedWord;

		@Override
		public void processLine(String input) {
			wordSearch.add(input);
		}

		@Override
		public Object finish() {
			int xmasCount = countXmas();
			LOG.info("XMAS appear {0} times.", xmasCount);
			return xmasCount;
		}

		private int countXmas() {
			int xmasCount = 0;
			for (int j = 0; j < wordSearch.size(); j++) {
				for (int i = 0; i < wordSearch.get(j).length(); i++) {
					xmasCount += countXmasFromCoordinates(new Point(i, j));
				}
			}
			return xmasCount;
		}

		protected int countXmasFromCoordinates(Point coordinates) {
			wantedWord = "XMAS";
			return getDirections().stream().mapToInt(d -> findWantedWord(coordinates, d, 0)).sum();
		}

		protected List<Point> getDirections() {
			List<Point> directions = new ArrayList<>();
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					if (x != 0 || y != 0) {
						directions.add(new Point(x, y));
					}
				}
			}
			return directions;
		}

		protected int findWantedWord(Point coordinates, Point direction, int characterIndex) {
			if (characterIndex >= wantedWord.length()) {
				return 1;
			}

			if (!coordinates.isInBounds(new Point(0, 0), new Point(wordSearch.get(0).length() - 1, wordSearch.size() - 1))) {
				return 0;
			}

			if (wordSearch.get(coordinates.get(Point.Y)).charAt(coordinates.get(Point.X)) != wantedWord.charAt(characterIndex)) {
				return 0;
			}

			Point nextCoordinates = new Point(coordinates);
			nextCoordinates.add(direction);
			return findWantedWord(nextCoordinates, direction, characterIndex + 1);
		}

	}

	private static class SecondPart extends FirstPart {

		@Override
		protected int countXmasFromCoordinates(Point coordinates) {
			if (wordSearch.get(coordinates.get(Point.Y)).charAt(coordinates.get(Point.X)) != 'A') {
				return 0;
			}

			wantedWord = "MAS";
			int masCount = 0;
			for (Point direction : getDirections()) {
				Point startCoordinates = new Point(coordinates);
				startCoordinates.add(direction.getReverse());
				masCount += findWantedWord(startCoordinates, direction, 0);
			}

			return masCount == 2 ? 1 : 0;
		}

		@Override
		protected List<Point> getDirections() {
			return super.getDirections().stream().filter(d -> d.get(Point.X) != 0 && d.get(Point.Y) != 0).collect(Collectors.toList());
		}
	}
}
