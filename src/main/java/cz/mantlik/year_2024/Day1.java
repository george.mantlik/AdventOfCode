package cz.mantlik.year_2024;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Day1 extends AbstractDayNg {

	public Day1() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		protected final List<Integer> leftList = new ArrayList<>();
		protected final List<Integer> rightList = new ArrayList<>();

		@Override
		public void processLine(String input) {
			String[] numbers = input.split("\\s+");
			leftList.add(Integer.parseInt(numbers[0]));
			rightList.add(Integer.parseInt(numbers[1]));
		}

		@Override
		public Object finish() {
			Collections.sort(leftList);
			Collections.sort(rightList);

			int totalDistance = 0;
			for (int i = 0; i < leftList.size(); i++) {
				totalDistance += Math.abs(leftList.get(i) - rightList.get(i));
			}

			LOG.info("The total distance between two lists of location IDs is {0}.", totalDistance);
			return totalDistance;
		}
	}

	private static class SecondPart extends FirstPart {


		@Override
		public Object finish() {
			long totalDistance = 0;
			for (Integer left : leftList) {
				totalDistance += left * rightList.stream().filter(right -> right.equals(left)).count();
			}

			LOG.info("The similarity score of two lists of location IDs is {0}.", totalDistance);
			return totalDistance;
		}

	}
}
