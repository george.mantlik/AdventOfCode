package cz.mantlik.year_2024;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;
import org.apache.commons.lang3.ArrayUtils;

public class Day2 extends AbstractDayNg {

	public Day2() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		private int safeReportCounter = 0;
		private int lastDirection;

		@Override
		public void processLine(String input) {
			String[] report = input.split(" ");
			if (processReport(report)) {
				safeReportCounter++;
			}
		}

		@Override
		public Object finish() {
			LOG.info("In this data, {0} reports are safe.", safeReportCounter);
			return safeReportCounter;
		}

		protected boolean processReport(String[] report) {
			lastDirection = 0;
			for (int i = 0; i < report.length - 1; i++) {
				if (!checkSafety(report[i], report[i + 1])) {
					return false;
				}
			}
			return true;
		}

		private boolean checkSafety(String firstLevel, String secondLevel) {
			int diff = Integer.parseInt(firstLevel) - Integer.parseInt(secondLevel);
			int absDiff = Math.abs(diff);
			if (absDiff < 1 || absDiff > 3) {
				return false;
			}

			int direction = diff / absDiff;
			if (lastDirection != 0 && direction != lastDirection) {
				return false;
			}

			lastDirection = direction;
			return true;
		}
	}

	private static class SecondPart extends FirstPart {

		@Override
		protected boolean processReport(String[] report) {
			for (int i = 0; i < report.length; i++) {
				if (super.processReport(ArrayUtils.remove(report, i))) {
					return true;
				}
			}

			return false;
		}
	}
}
