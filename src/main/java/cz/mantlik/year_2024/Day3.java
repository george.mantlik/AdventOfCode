package cz.mantlik.year_2024;

import cz.mantlik.common.AbstractDayNg;
import cz.mantlik.common.DayPart;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day3 extends AbstractDayNg {

	public Day3() {
		super(new FirstPart(), new SecondPart());
	}

	private static class FirstPart implements DayPart {

		protected int mulSum = 0;

		@Override
		public void processLine(String input) {
			Pattern mulPattern = Pattern.compile(getRegex());
			Matcher matcher = mulPattern.matcher(input);
			while (matcher.find()) {
				processInstruction(matcher);
			}
		}

		protected String getRegex() {
			return "mul\\(([0-9]{1,3}),([0-9]{1,3})\\)";
		}

		protected void processInstruction(Matcher matcher) {
			mulSum += Integer.parseInt(matcher.group(1)) * Integer.parseInt(matcher.group(2));
		}

		@Override
		public Object finish() {
			LOG.info(getMessage(), mulSum);
			return mulSum;
		}

		protected String getMessage() {
			return "Sum of all of the results of the multiplications is {0}.";
		}

	}

	private static class SecondPart extends FirstPart {

		private static final String DO_REGEX = "(do\\(\\))";
		private static final String DONT_REGEX = "(don't\\(\\))";

		private boolean mulEnabled = true;

		@Override
		protected String getRegex() {
			return super.getRegex() + "|" + DO_REGEX + "|" + DONT_REGEX;
		}

		@Override
		protected void processInstruction(Matcher matcher) {
			String instruction = matcher.group();
			if (instruction.matches(DO_REGEX)) {
				LOG.debug("Enabling mul instructions.");
				mulEnabled = true;
			} else if (instruction.matches(DONT_REGEX)) {
				LOG.debug("Disabling mul instructions.");
				mulEnabled = false;
			} else if (mulEnabled) {
				LOG.debug("Executing instruction {0}.", instruction);
				super.processInstruction(matcher);
			} else {
				LOG.debug("Skipping instruction {0}.", instruction);
			}
		}

		@Override
		public String getMessage() {
			return "Sum of all of the results of just the enabled multiplications is {0}.";
		}

	}

}
