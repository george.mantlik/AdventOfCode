package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;

import java.util.Arrays;
import java.util.List;

public class Day2 extends AbstractDay {

	private int total = 0;
	private List<String> outcomes;

	public Day2() {
		super(false);
		outcomes = Arrays.asList("B X", "C Y", "A Z", "A X", "B Y", "C Z", "C X", "A Y", "B Z");
	}

	@Override
	protected void processLineFirstPart(String input) {
		total += outcomes.indexOf(input) + 1;
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("If everything goes exactly according to strategy guide, the total score will be {0}.", total);
	}

	@Override
	protected void setupSecondPart() {
		total = 0;
		outcomes = Arrays.asList("B X", "C X", "A X", "A Y", "B Y", "C Y", "C Z", "A Z", "B Z");
	}

	@Override
	protected void processLineSecondPart(String input) {
		total += outcomes.indexOf(input) + 1;
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("If everything goes exactly according to strategy guide, the total score will be {0}.", total);
	}
}
