package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day7 extends AbstractDay {

	private static final String CD_PREFIX = "$ cd ";
	private static final String LS_PREFIX = "$ ls";

	private final List<Node> directories = new ArrayList<>();
	private final Node topDirectory = new Node("/", null);
	private Node currentDirectory;

	public Day7() {
		super(false);
		directories.add(topDirectory);
	}

	@Override
	protected void processLineFirstPart(String input) {
		if (input.startsWith(CD_PREFIX)) {
			currentDirectory = translateDirectory(input.substring(CD_PREFIX.length()));
		} else if (!input.startsWith(LS_PREFIX)) {
			createNode(input);
		}
	}

	private Node translateDirectory(String input) {
		if (input.equals("/")) {
			return topDirectory;
		} else if (input.equals("..")) {
			return currentDirectory.getParent();
		} else {
			return currentDirectory.getChild(input);
		}
	}

	private void createNode(String input) {
		String[] parts = input.split(" ");
		Node node = new Node(parts[1], currentDirectory);
		if (input.startsWith("dir")) {
			directories.add(node);
		} else {
			node.setSize(Integer.parseInt(parts[0]));
		}
	}

	@Override
	protected void finishFirstPart() {
		int sizeToSum = 100000;
		int sum = directories.stream().mapToInt(Node::getSize).filter(s -> s <= sizeToSum).sum();
		LOG.info("The sum of total sizes of directories with a total size of at most {0} is {1}.", sizeToSum, sum);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Nothing to do here
	}

	@Override
	protected void finishSecondPart() {
		int availableSpace = 70000000 - 30000000;
		int spaceToFree = topDirectory.getSize() - availableSpace;
		int size = directories.stream().mapToInt(Node::getSize).filter(s -> s >= spaceToFree).sorted().findFirst().orElse(0);
		LOG.info("The total size of the smallest directory that, if deleted, would free up enough space to run the update is {0}.", size);
	}

	private static final class Node {

		private final String name;
		private final Node parent;
		private int size = Integer.MAX_VALUE;
		private final Map<String, Node> subNodes = new HashMap<>();

		private Node(String name, Node parent) {
			this.name = name;
			this.parent = parent;
			if (parent != null) {
				parent.addChild(this);
			}
		}

		public Node getParent() {
			return parent == null ? this : parent;
		}

		public void setSize(int size) {
			this.size = size;
		}

		public int getSize() {
			if (size == Integer.MAX_VALUE) {
				size = 0;
				for (Node subNode : subNodes.values()) {
					size += subNode.getSize();
				}
			}

			return size;
		}

		public void addChild(Node child) {
			subNodes.put(child.name, child);
		}

		public Node getChild(String name) {
			Node child = subNodes.get(name);
			return child == null ? this : child;
		}
	}
}
