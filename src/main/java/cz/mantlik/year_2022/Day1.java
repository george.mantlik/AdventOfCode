package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Day1 extends AbstractDay {

	private int maxCalories = 0;
	private int currentTotal = 0;

	private final List<Integer> topThree = new ArrayList<>(Arrays.asList(0, 0, 0));

	public Day1() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		if (input.isEmpty()) {
			processCurrentFirstPart();
		} else {
			currentTotal += Integer.parseInt(input);
		}
	}

	private void processCurrentFirstPart() {
		maxCalories = Math.max(maxCalories, currentTotal);
		currentTotal = 0;
	}

	@Override
	protected void finishFirstPart() {
		processCurrentFirstPart();
		LOG.info("Elf carrying the most Calories is carrying a total of {0} Calories.", maxCalories);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		if (input.isEmpty()) {
			processCurrentSecondPart();
		} else {
			currentTotal += Integer.parseInt(input);
		}
	}

	private void processCurrentSecondPart() {
		if (currentTotal > topThree.get(2)) {
			topThree.remove(2);
			topThree.add(currentTotal);
			topThree.sort(Comparator.reverseOrder());
		}
		currentTotal = 0;
	}

	@Override
	protected void finishSecondPart() {
		processCurrentSecondPart();
		LOG.info("Top three Elves carrying the most Calories are carrying a total of {0} Calories.", topThree.stream().mapToInt(Integer::intValue).sum());
	}
}
