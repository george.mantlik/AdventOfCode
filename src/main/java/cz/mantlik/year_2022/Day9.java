package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;
import cz.mantlik.puzzleutils.Point;

import java.util.*;

import static cz.mantlik.puzzleutils.Point.X;
import static cz.mantlik.puzzleutils.Point.Y;

public class Day9 extends AbstractDay {

	private final Map<String, Point> directions = new HashMap<>();
	private final Set<Point> visitedPoints = new HashSet<>();
	private final Point head = new Point(0, 0);
	private final Point tail = new Point(0, 0);

	private final List<Point> knots = new ArrayList<>();

	public Day9() {
		super(false);
		directions.put("R", new Point(1, 0));
		directions.put("L", new Point(-1, 0));
		directions.put("U", new Point(0, 1));
		directions.put("D", new Point(0, -1));
	}

	@Override
	protected void processLineFirstPart(String input) {
		String[] parts = input.split(" ");
		for (int i = 0; i < Integer.parseInt(parts[1]); i++) {
			head.add(directions.get(parts[0]));
			moveKnot(tail, head);
			visitedPoints.add(new Point(tail));
		}
	}

	private void moveKnot(Point toMove, Point previous) {
		if (isNeighbor(previous.get(X), toMove.get(X)) && isNeighbor(previous.get(Y), toMove.get(Y))) {
			return;
		}
		if (isNeighbor(previous.get(X), toMove.get(X))) {
			toMove.set(X, previous.get(X));
		}
		if (isNeighbor(previous.get(Y), toMove.get(Y))) {
			toMove.set(Y, previous.get(Y));
		}
		toMove.add(getDirection(toMove, previous));
	}

	private boolean isNeighbor(int a, int b) {
		return Math.abs(a - b) <= 1;
	}

	private Point getDirection(Point toMove, Point previous) {
		return new Point(getDirectionCoordinate(toMove.get(X), previous.get(X)), getDirectionCoordinate(toMove.get(Y), previous.get(Y)));
	}

	private int getDirectionCoordinate(int toMove, int previous) {
		if (toMove == previous) {
			return 0;
		}
		return (previous - toMove) / Math.abs(previous - toMove);
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("The tail of the rope visits at least once {0} positions.", visitedPoints.size());
	}

	@Override
	protected void setupSecondPart() {
		visitedPoints.clear();
		for (int i = 0; i < 10; i++) {
			knots.add(new Point(0, 0));
		}
	}

	@Override
	protected void processLineSecondPart(String input) {
		String[] parts = input.split(" ");
		for (int i = 0; i < Integer.parseInt(parts[1]); i++) {
			knots.get(0).add(directions.get(parts[0]));
			for (int j = 1; j < knots.size(); j++) {
				moveKnot(knots.get(j), knots.get(j - 1));
			}
			visitedPoints.add(new Point(knots.get(knots.size() - 1)));
		}
	}

	@Override
	protected void finishSecondPart() {
		finishFirstPart();
	}
}
