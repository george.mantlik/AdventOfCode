package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;

import java.util.function.Function;

public class Day6 extends AbstractDay {

	private int markerSize = 4;
	// Function counting recursion step size needed because of stack overflow in part two :'(
	private Function<String, Integer> stepSizeFunction = str -> 1;

	public Day6() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		int markerEnd = findMarkerEnd(input);
		LOG.info("First start-of-packet marker is detected after processing {0} characters.", markerEnd);
	}

	private int findMarkerEnd(String input) {
		String potentialMarker = input.substring(0, markerSize);
		if (potentialMarker.chars().distinct().count() < markerSize) {
			int stepSize = stepSizeFunction.apply(potentialMarker);
			return stepSize + findMarkerEnd(input.substring(stepSize));
		} else {
			return markerSize;
		}
	}

	@Override
	protected void finishFirstPart() {
		// Nothing to do
	}

	@Override
	protected void setupSecondPart() {
		markerSize = 14;
		stepSizeFunction = str -> {
			int stepSize = 1;
			while (stepSize < str.length() - 1 && str.substring(stepSize + 1).contains("" + str.charAt(stepSize))) {
				stepSize++;
			}
			return stepSize;
		};
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLineFirstPart(input);
	}

	@Override
	protected void finishSecondPart() {
		// Nothing to do here
	}
}
