package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.UnaryOperator;

public class Day11 extends AbstractDay {

	private final List<Monkey> monkeys = new ArrayList<>();
	private int numberOfRounds = 20;
	private UnaryOperator<Long> worryLevelManager = worryLevel -> worryLevel / 3;

	public Day11() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		if (input.startsWith("Monkey")) {
			monkeys.add(new Monkey(monkeys));
		} else {
			Monkey currentMonkey = monkeys.get(monkeys.size() - 1);
			String[] parts = input.trim().split(": ");
			switch (parts[0]) {
				case "Starting items":
					currentMonkey.setUpItems(parts[1].split(", "), worryLevelManager);
					break;
				case "Operation":
					currentMonkey.setOperation(getOperation(parts[1]));
					break;
				case "Test":
					currentMonkey.setTestDivisor(Long.parseLong(removePrefix(parts[1], "divisible by ")));
					break;
				case "If true":
					currentMonkey.setTrueFriend(getThrowTo(parts));
					break;
				case "If false":
					currentMonkey.setFalseFriend(getThrowTo(parts));
					break;
				default:
					// Empty line, nothing to do
			}
		}
	}

	private Consumer<Item> getOperation(String input) {
		String[] parts = removePrefix(input, "new = old ").split(" ");
		if (parts[1].equals("old")) {
			return Item::square;
		} else {
			long operand = Long.parseLong(parts[1]);
			return parts[0].equals("+") ? item -> item.add(operand) : item -> item.multiply(operand);
		}
	}

	private String removePrefix(String string, String prefix) {
		return string.replace(prefix, "");
	}

	private int getThrowTo(String[] inputParts) {
		return Integer.parseInt(removePrefix(inputParts[1] ,"throw to monkey "));
	}

	@Override
	protected void finishFirstPart() {
		for (int i = 0; i < numberOfRounds; i++) {
			for (Monkey monkey : monkeys) {
				monkey.inspectItems();
			}
		}

		for (int i = 0; i < monkeys.size(); i++) {
			LOG.info("Monkey {0} inspected items {1} times.", i, monkeys.get(i).getInspections());
		}
		long monkeyBusiness = monkeys.stream().map(Monkey::getInspections).sorted(Comparator.reverseOrder()).limit(2).reduce(1L, (a, b) -> a * b);
		LOG.info("The level of monkey business after {0} rounds of stuff-slinging simian shenanigans is {1}.", numberOfRounds, monkeyBusiness);
	}

	@Override
	protected void setupSecondPart() {
		long monkeyTestMultiple = monkeys.stream().map(Monkey::getTestDivisor).reduce(1L, (a, b) -> a * b);
		worryLevelManager = worryLevel -> worryLevel % monkeyTestMultiple;
		monkeys.clear();
		numberOfRounds = 10000;
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLineFirstPart(input);
	}

	@Override
	protected void finishSecondPart() {
		finishFirstPart();
	}

	private static final class Item {

		private long worryLevel;
		private final UnaryOperator<Long> worryLevelManager;

		public Item(long worryLevel, UnaryOperator<Long> worryLevelManager) {
			this.worryLevel = worryLevel;
			this.worryLevelManager = worryLevelManager;
		}

		public void add(long value) {
			worryLevel += value;
		}

		public void multiply(long value) {
			worryLevel *= value;
		}

		public void square() {
			worryLevel *= worryLevel;
		}

		public boolean test(long divisor) {
			worryLevel = worryLevelManager.apply(worryLevel);
			return worryLevel % divisor == 0;
		}
	}

	private static final class Monkey {

		private final List<Monkey> friends;
		private final Queue<Item> items = new LinkedList<>();
		private Consumer<Item> operation;
		private long testDivisor;
		private int trueFriend;
		private int falseFriend;

		private long inspections = 0;

		public Monkey(List<Monkey> friends) {
			this.friends = friends;
		}

		public void setUpItems(String[] itemLevels, UnaryOperator<Long> worryLevelManager) {
			for (String worryLevel : itemLevels) {
				addItem(new Item(Long.parseLong(worryLevel), worryLevelManager));
			}
		}

		public void addItem(Item item) {
			items.add(item);
		}

		public void setOperation(Consumer<Item> operation) {
			this.operation = operation;
		}

		public long getTestDivisor() {
			return testDivisor;
		}

		public void setTestDivisor(long testDivisor) {
			this.testDivisor = testDivisor;
		}

		public void setTrueFriend(int trueFriend) {
			this.trueFriend = trueFriend;
		}

		public void setFalseFriend(int falseFriend) {
			this.falseFriend = falseFriend;
		}

		public void inspectItems() {
			while (!items.isEmpty()) {
				Item inspectedItem = items.poll();
				operation.accept(inspectedItem);
				int throwTo = inspectedItem.test(testDivisor) ? trueFriend : falseFriend;
				friends.get(throwTo).addItem(inspectedItem);
				inspections++;
			}
		}

		public long getInspections() {
			return inspections;
		}

	}
}
