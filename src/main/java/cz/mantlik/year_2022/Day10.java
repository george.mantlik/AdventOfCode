package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;
import cz.mantlik.puzzleutils.PuzzleLogger;

public class Day10 extends AbstractDay {

	private static final String ADD_PREFIX = "addx ";

	private int x = 1;
	private int cycleCounter = 0;
	private int strengthSum = 0;
	private int checkCycle = 20;

	private final StringBuilder resultBuilder = new StringBuilder();

	public Day10() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		checkStrength();
		if (input.startsWith(ADD_PREFIX)) {
			checkStrength();
			x += Integer.parseInt(input.replace(ADD_PREFIX, ""));
		}
	}

	private void checkStrength() {
		if (++cycleCounter >= checkCycle) {
			strengthSum += checkCycle * x;
			checkCycle += 40;
		}
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("The sum of the signal strengths during the 20th, 60th, 100th, 140th, 180th, and 220th cycles is {0}.", strengthSum);
	}

	@Override
	protected void setupSecondPart() {
		x = 1;
		cycleCounter = 0;
	}

	@Override
	protected void processLineSecondPart(String input) {
		draw();
		if (input.startsWith(ADD_PREFIX)) {
			draw();
			x += Integer.parseInt(input.replace(ADD_PREFIX, ""));
		}
	}

	private void draw() {
		int index = cycleCounter++ % 40;
		if (index == 0) {
			resultBuilder.append("\n");
		}
		if (Math.abs(x - index) <= 1) {
			resultBuilder.append(PuzzleLogger.DRAW_LINE);
		} else {
			resultBuilder.append(PuzzleLogger.DRAW_SPACE);
		}
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("On the CRT appear eight letters:{0}", resultBuilder.toString());
	}
}
