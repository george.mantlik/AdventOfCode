package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;

import java.util.List;
import java.util.stream.Collectors;

public class Day3 extends AbstractDay {

	private int prioritySum = 0;

	private List<Integer> badgeTypes = null;

	public Day3() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		int repetition = input.substring(0, input.length() / 2).chars()
				.filter(c -> input.substring(input.length() / 2).chars().anyMatch(c2 -> c2 == c))
				.findFirst()
				.orElse(0);
		prioritySum += repetition > 90 ? repetition - 96 : repetition - 38;
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("The sum of the priorities of item types that appear in both compartments is {0}.", prioritySum);
	}

	@Override
	protected void setupSecondPart() {
		prioritySum = 0;
	}

	@Override
	protected void processLineSecondPart(String input) {
		if (badgeTypes == null) {
			badgeTypes = input.chars().boxed().collect(Collectors.toList());
		} else {
			badgeTypes = badgeTypes.stream().filter(c -> input.chars().anyMatch(c2 -> c2 == c)).distinct().collect(Collectors.toList());
			if (badgeTypes.size() == 1) {
				int type = badgeTypes.get(0);
				prioritySum += type > 90 ? type - 96 : type - 38;
				badgeTypes = null;
			}
		}
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("The sum of the priorities of item types that correspond to the badges is {0}.", prioritySum);
	}
}
