package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day4 extends AbstractDay {

	private Elf elf1;
	private Elf elf2;

	private int count = 0;

	public Day4() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		parseElves(input);
		if (elf1.contains(elf2) || elf2.contains(elf1)) {
			count++;
		}
	}

	private void parseElves(String input) {
		List<Integer> parts = Arrays.stream(input.split("[-,]")).map(Integer::parseInt).collect(Collectors.toList());
		elf1 = new Elf(parts.get(0), parts.get(1));
		elf2 = new Elf(parts.get(2), parts.get(3));
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("One range fully contains the other in {0} assignments.", count);
	}

	@Override
	protected void setupSecondPart() {
		count = 0;
	}

	@Override
	protected void processLineSecondPart(String input) {
		parseElves(input);
		if (elf1.contains(elf2.getStartElf()) || elf2.contains(elf1.getStartElf())) {
			count++;
		}
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("Ranges overlap in {0} assignments.", count);
	}

	private static final class Elf {

		private final int start;
		private final int end;

		public Elf(int start, int end) {
			this.start = start;
			this.end = end;
		}

		public boolean contains(Elf other) {
			return start <= other.start && end >= other.end;
		}

		public Elf getStartElf() {
			return new Elf(start, start);
		}

	}
}
