package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class Day25 extends AbstractDay {

	private long fuelRequirementsSum = 0;

	private final SnafuConverter snafuConverter = new SnafuConverter();

	public Day25() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		fuelRequirementsSum += snafuConverter.toDecimal(input);
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("Fuel requirements sum is {0}.", fuelRequirementsSum);
		LOG.info("SNAFU representation of the fuel requirements sum is {0}.", snafuConverter.fromDecimal(fuelRequirementsSum));
	}

	@Override
	protected void setupSecondPart() {
		// Not enough stars for part two :'(
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Not enough stars for part two :'(
	}

	@Override
	protected void finishSecondPart() {
		// Not enough stars for part two :'(
	}

	private static final class SnafuConverter {

		private final Map<Character, Character> quinaryToSnafu = new HashMap<>();

		public SnafuConverter() {
			quinaryToSnafu.put('3', '=');
			quinaryToSnafu.put('4', '-');
			quinaryToSnafu.put('5', '0');
		}

		public String fromDecimal(long value) {
			String reversed = decimalToReversedQuinary(value);
			for (int i = 0; i < reversed.length(); i++) {
				Character digit = reversed.charAt(i);
				Character replacement = quinaryToSnafu.get(digit);
				if (replacement != null) {
					char nextDigit = (char) (reversed.charAt(i + 1) + 1);
					reversed = reversed.substring(0, i) + replacement + nextDigit + reversed.substring(i + 2);
				}
			}
			return StringUtils.reverse(reversed).replaceFirst("^0+(?!$)", "");
		}

		private String decimalToReversedQuinary(long decimal) {
			StringBuilder reversedQuinary = new StringBuilder();
			while (decimal >= 5) {
				reversedQuinary.append(decimal % 5);
				decimal /= 5;
			}
			reversedQuinary.append(decimal);
			return reversedQuinary.toString() + "0";
		}

		public long toDecimal(String snafu) {
			long decimal = 0;
			long digitRank = 1;
			snafu = StringUtils.reverse(snafu);
			for (int i = 0; i < snafu.length(); i++) {
				decimal += digitRank * getQuinaryDigit(snafu.charAt(i));
				digitRank *= 5;
			}
			return decimal;
		}

		private int getQuinaryDigit(Character snafuDigit) {
			switch (snafuDigit) {
				case '-':
					return -1;
				case '=':
					return -2;
				default:
					return Integer.parseInt("" + snafuDigit);
			}
		}

	}
}
