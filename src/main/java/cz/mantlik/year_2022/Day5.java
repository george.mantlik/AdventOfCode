package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class Day5 extends AbstractDay {

	private Crane crane = new Crane();
	private final List<String> stackLines = new ArrayList<>();

	public Day5() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		if (input.isEmpty()) {
			return;
		}
		if (crane.isInitialized()) {
			List<Integer> instructions = getInstructions(input);
			crane.move(instructions);
		} else if (input.startsWith(" 1")) {
			crane.initStacks(input);
			for (int i = stackLines.size() - 1; i >= 0; i--) {
				crane.incrementStacks(stackLines.get(i));
			}
		} else {
			stackLines.add(input);
		}
	}

	private List<Integer> getInstructions(String input) {
		return Arrays.stream(input.split("move | from | to ")).filter(StringUtils::isNotEmpty).map(Integer::parseInt).collect(Collectors.toList());
	}

	@Override
	protected void finishFirstPart() {
		String crates = crane.getTopCrates();
		LOG.info("After the rearrangement procedure completes, on top of each stack end up crates {0}.", crates);
	}

	@Override
	protected void setupSecondPart() {
		crane = new Crane9001();
		stackLines.clear();
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLineFirstPart(input);
	}

	@Override
	protected void finishSecondPart() {
		finishFirstPart();
	}

	private static class Crane {

		protected final List<Stack<String>> stacks = new ArrayList<>();

		public boolean isInitialized() {
			return !stacks.isEmpty();
		}

		public void initStacks(String numbersLine) {
			int stackCount = Integer.parseInt(numbersLine.substring(numbersLine.length() - 1));

			for (int i =0; i < stackCount; i++) {
				stacks.add(new Stack<>());
			}
		}

		public void incrementStacks(String inputLine) {
			for (int i = 1; i < stacks.size() * 4; i += 4) {
				if (i >= inputLine.length()) {
					return;
				}
				String character = inputLine.substring(i, i + 1);
				if (character.matches("[A-Z]")) {
					stacks.get(i / 4).push(character);
				}
			}
		}

		public void move(List<Integer> instructions) {
			for (int i = 0; i < instructions.get(0); i++) {
				stacks.get(instructions.get(2) - 1).push(stacks.get(instructions.get(1) - 1).pop());
			}
		}

		public String getTopCrates() {
			return stacks.stream().map(Stack::peek).reduce(String::concat).orElse("");
		}

	}

	private static final class Crane9001 extends Crane {

		@Override
		public void move(List<Integer> instructions) {
			Stack<String> mediator = new Stack<>();
			for (int i = 0; i < instructions.get(0); i++) {
				mediator.push(stacks.get(instructions.get(1) - 1).pop());
			}
			for (int i = 0; i < instructions.get(0); i++) {
				stacks.get(instructions.get(2) - 1).push(mediator.pop());
			}
		}

	}
}
