package cz.mantlik.year_2022;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.List;

public class Day8 extends AbstractDay {

	private final List<String> treeLines = new ArrayList<>();
	private int currentTreeHeight;

	public Day8() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		treeLines.add(input);
	}

	@Override
	protected void finishFirstPart() {
		int visibleTrees = 0;
		for (int i = 0; i < treeLines.size(); i++) {
			String horizontalTreeLine = treeLines.get(i);
			for (int j = 0; j < horizontalTreeLine.length(); j++) {
				currentTreeHeight = getHeight(horizontalTreeLine, j);
				int finalJ = j;
				String verticalLine = treeLines.stream().map(s -> "" + s.charAt(finalJ)).reduce(String::concat).orElse("");
				if (checkTreeline(horizontalTreeLine, j) || checkTreeline(verticalLine, i)) {
					visibleTrees++;
				}
			}
		}
		LOG.info("There are {0} trees visible from outside the grid.", visibleTrees);
	}

	private boolean checkTreeline(String treeLine, int treePosition) {
		return checkPartOfTreeLine(treeLine, 0, treePosition) || checkPartOfTreeLine(treeLine, treePosition + 1, treeLine.length());
	}

	private boolean checkPartOfTreeLine(String treeLine, int start, int end) {
		for (int i = start; i < end; i++) {
			if (getHeight(treeLine, i) >= currentTreeHeight) {
				return false;
			}
		}
		return true;
	}

	private int getHeight(String treeLine, int treePosition) {
		return Integer.parseInt("" + treeLine.charAt(treePosition));
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Nothing to do here
	}

	@Override
	protected void finishSecondPart() {
		int highestScenicScore = 0;
		for (int i = 0; i < treeLines.size(); i++) {
			String horizontalTreeLine = treeLines.get(i);
			for (int j = 0; j < horizontalTreeLine.length(); j++) {
				currentTreeHeight = getHeight(horizontalTreeLine, j);
				int finalJ = j;
				String verticalLine = treeLines.stream().map(s -> "" + s.charAt(finalJ)).reduce(String::concat).orElse("");
				int scenicScore = findScenicScoreForLine(horizontalTreeLine, j) * findScenicScoreForLine(verticalLine, i);
				highestScenicScore = Math.max(highestScenicScore, scenicScore);
			}
		}
		LOG.info("The highest scenic score possible for any tree is {0}.", highestScenicScore);
	}

	private int findScenicScoreForLine(String treeLine, int treePosition) {
		return findTreeCountReversed(treeLine, treePosition) * findTreeCount(treeLine, treePosition);
	}

	private int findTreeCount(String treeLine, int treePosition) {
		int treeCount = 0;
		for (int i = treePosition + 1; i < treeLine.length(); i++) {
			treeCount++;
			if (getHeight(treeLine, i) >= currentTreeHeight) {
				break;
			}
		}
		return treeCount;
	}

	private int findTreeCountReversed(String treeLine, int treePosition) {
		int treeCount = 0;
		for (int i = treePosition - 1; i >= 0; i--) {
			treeCount++;
			if (getHeight(treeLine, i) >= currentTreeHeight) {
				break;
			}
		}
		return treeCount;
	}
}
