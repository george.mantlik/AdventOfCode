package cz.mantlik.common;

import cz.mantlik.puzzleutils.PuzzleReader;

import java.io.File;
import java.util.StringJoiner;

public class DayRunner {

	private static final String FILE_EXTENSION = ".txt";
	private static final String FILE_TEST = "test";
	private static final String FILE_MAIN = "main";

	private final PuzzleReader puzzleReader = new PuzzleReader();
	private Class<?> dayClass;
	private DayPart dayPart;

	public void setDayPart(DayPart dayPart) {
		this.dayPart = dayPart;
		this.dayClass = dayPart.getClass().getEnclosingClass();
	}

	public Object run(boolean test) {
		return run(test ? FILE_TEST : FILE_MAIN);
	}

	public Object run(String fileName) {
		if (dayPart == null) {
			throw new RuntimeException("DayPart was not set.");
		}
		if (dayClass == null) {
			throw new RuntimeException("DayPart should be defined inside any Day class.");
		}
		puzzleReader.readFile(convertFileName(fileName), dayPart::processLine);
		return dayPart.finish();
	}

	private String convertFileName(String fileName) {
		StringJoiner fileNameJoiner = new StringJoiner(File.separator);
		fileNameJoiner.add(getYear());
		fileNameJoiner.add(dayClass.getSimpleName().toLowerCase());
		fileNameJoiner.add(fileName + FILE_EXTENSION);
		return fileNameJoiner.toString();
	}

	private String getYear() {
		String wholePackage = dayClass.getPackage().getName();
		int lastDotIndex = wholePackage.lastIndexOf('.');
		return lastDotIndex >= 0 ? wholePackage.substring(lastDotIndex + 1) : wholePackage;
	}

}
