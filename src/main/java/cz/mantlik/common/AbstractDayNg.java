package cz.mantlik.common;

import cz.mantlik.puzzleutils.PuzzleLogger;

public class AbstractDayNg {

	protected static final PuzzleLogger LOG = PuzzleLogger.getLogger(AbstractDayNg.class.getName());

	private final DayPart firstPart;
	private final DayPart secondPart;

	public AbstractDayNg(DayPart firstPart, DayPart secondPart) {
		this.firstPart = firstPart;
		this.secondPart = secondPart;
	}

	public DayPart getFirstPart() {
		return firstPart;
	}

	public DayPart getSecondPart() {
		return secondPart;
	}

}
