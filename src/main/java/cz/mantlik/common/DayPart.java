package cz.mantlik.common;

public interface DayPart {

	void processLine(String input);

	Object finish();

}
