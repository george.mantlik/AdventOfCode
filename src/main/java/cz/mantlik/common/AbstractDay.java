package cz.mantlik.common;

import cz.mantlik.puzzleutils.PuzzleLogger;
import cz.mantlik.puzzleutils.PuzzleReader;

import java.io.File;
import java.util.function.Consumer;

public abstract class AbstractDay {

	protected static final PuzzleLogger LOG = PuzzleLogger.getLogger(AbstractDay.class.getName());
	private static final String FILE_EXTENSION = ".txt";
	private static final String FILE_TEST = "test";
	private static final String FILE_MAIN = "main";

	private final PuzzleReader puzzleReader = new PuzzleReader();
	private final String fileName;

	private int partCounter = 1;

	public AbstractDay(boolean test) {
		this(test ? FILE_TEST : FILE_MAIN);
	}

	public AbstractDay(String name) {
		this.fileName = getClass().getPackage().getName() + File.separator + getClass().getSimpleName().toLowerCase() + File.separator + name + FILE_EXTENSION;
	}

	public void run() {
		runPart(this::processLineFirstPart, this::finishFirstPart);

		setupSecondPart();

		runPart(this::processLineSecondPart, this::finishSecondPart);
	}

	protected void runPart(Consumer<String> lineAction, Runnable finishAction) {
		LOG.info("---------------------- Part {0} ----------------------", partCounter++);
		puzzleReader.readFile(fileName, lineAction);
		finishAction.run();
	}

	protected abstract void processLineFirstPart(String input);

	protected abstract void finishFirstPart();

	protected abstract void setupSecondPart();

	protected abstract void processLineSecondPart(String input);

	protected abstract void finishSecondPart();

}
