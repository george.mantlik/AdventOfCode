package cz.mantlik.year_2020;

import cz.mantlik.common.AbstractDay;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day2 extends AbstractDay {

	private int validPasswordCount = 0;

	public Day2() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		List<String> inputParts = Arrays.asList(input.split(" "));
		List<Integer> bounds = Stream.of(inputParts.get(0).split("-")).map(Integer::parseInt).collect(Collectors.toList());
		char condition = inputParts.get(1).charAt(0);
		String password = inputParts.get(2);

		if (isPasswordValid(password, condition, bounds.get(0), bounds.get(1))) {
			validPasswordCount += 1;
		}
	}

	private boolean isPasswordValid(String password, char condition, int lowerBound, int upperBound) {
		long conditionCount = password.chars().filter(ch -> ch == condition).count();
		return conditionCount >= lowerBound && conditionCount <= upperBound;
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("{0} passwords are valid according to policies.", validPasswordCount);
	}

	@Override
	protected void setupSecondPart() {
		validPasswordCount = 0;
	}

	@Override
	protected void processLineSecondPart(String input) {
		List<String> inputParts = Arrays.asList(input.split(" "));
		List<Integer> bounds = Stream.of(inputParts.get(0).split("-")).map(Integer::parseInt).collect(Collectors.toList());
		char condition = inputParts.get(1).charAt(0);
		String password = inputParts.get(2);

		if (isPasswordReallyValid(password, condition, bounds.get(0), bounds.get(1))) {
			validPasswordCount += 1;
		}
	}

	private boolean isPasswordReallyValid(String password, char condition, int lowerBound, int upperBound) {
		return (password.charAt(lowerBound - 1) == condition && password.charAt(upperBound - 1) != condition) ||
				(password.charAt(lowerBound - 1) != condition && password.charAt(upperBound - 1) == condition);
	}

	@Override
	protected void finishSecondPart() {
		finishFirstPart();
	}
}
