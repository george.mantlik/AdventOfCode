package cz.mantlik.year_2020;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Day5 extends AbstractDay {

	private int highestSeatID = 0;

	private final List<Integer> seatIds = new ArrayList<>();

	public Day5() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		int seatID = getSeatID(input);
		if (seatID > highestSeatID) {
			highestSeatID = seatID;
		}
	}

	private int getRow(String input) {
		return Integer.parseInt(input.substring(0, 7).replace("F", "0").replace("B", "1"), 2);
	}

	private int getCol(String input) {
		return Integer.parseInt(input.substring(7).replace("L", "0").replace("R", "1"), 2);
	}

	private int getSeatID(String input) {
		return getRow(input) * 8 + getCol(input);
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("Highest seat ID is {0}.", highestSeatID);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		seatIds.add(getSeatID(input));
	}

	@Override
	protected void finishSecondPart() {
		Collections.sort(seatIds);
		Integer lastSeatID = null;
		for (int seatID : seatIds) {
			if (lastSeatID != null && seatID - lastSeatID > 1) {
				LOG.info("Missing seat ID {0}.", seatID - 1);
				break;
			}
			lastSeatID = seatID;
		}
	}
}
