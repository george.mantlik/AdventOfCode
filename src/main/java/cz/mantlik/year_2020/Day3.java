package cz.mantlik.year_2020;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.List;

public class Day3 extends AbstractDay {

	private static final int SHIFT = 3;
	private int position = 0;
	private int treeCount = 0;

	private static final int[] SHIFTS = {1, 3, 5, 7, 1};
	private final List<Integer> positions = new ArrayList<>();
	private final List<Integer> treeCounts = new ArrayList<>();
	private int lineCount = 0;

	public Day3() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		if (input.charAt(position) == '#') {
			treeCount += 1;
		}
		position = (position + SHIFT) % input.length();
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("Traversing the map using this slope would cause you to encounter {0} trees.", treeCount);
	}

	@Override
	protected void setupSecondPart() {
		for (int i = 0; i < 5; i++) {
			positions.add(0);
			treeCounts.add(0);
		}
	}

	@Override
	protected void processLineSecondPart(String input) {
		for (int i = 0; i < 4; i++) {
			checkTree(input, i);
		}
		if (lineCount % 2 == 0) {
			checkTree(input, 4);
		}
		lineCount += 1;
	}

	private void checkTree(String input, int index) {
		int pos = positions.get(index);
		int tree = treeCounts.get(index);
		if (input.charAt(pos) == '#') {
			treeCounts.set(index, tree + 1);
		}
		positions.set(index, (pos + SHIFTS[index]) % input.length());
	}

	@Override
	protected void finishSecondPart() {
		long product = (long) treeCounts.get(0) * treeCounts.get(1) * treeCounts.get(2) * treeCounts.get(3) * treeCounts.get(4);
		LOG.info("These slopes would find {0}, {1}, {2}, {3}, and {4} tree(s) respectively; multiplied together, these produce the answer {5}.",
				treeCounts.get(0), treeCounts.get(1), treeCounts.get(2), treeCounts.get(3), treeCounts.get(4), product);
	}
}
