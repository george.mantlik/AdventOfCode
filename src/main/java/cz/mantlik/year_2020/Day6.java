package cz.mantlik.year_2020;

import cz.mantlik.common.AbstractDay;

import java.util.*;

public class Day6 extends AbstractDay {

	private final Set<String> positiveAnswers = new HashSet<>();
	private int positiveAnswerCount = 0;
	private boolean firstPerson = true;

	public Day6() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		if (input.isEmpty()) {
			positiveAnswerCount += positiveAnswers.size();
			positiveAnswers.clear();
		} else {
			positiveAnswers.addAll(Arrays.asList(input.split("")));
		}
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("Sum of counts of questions to which anyone answered 'yes' is {0}", positiveAnswerCount);
	}

	@Override
	protected void setupSecondPart() {
		positiveAnswerCount = 0;
	}

	@Override
	protected void processLineSecondPart(String input) {
		if (input.isEmpty()) {
			positiveAnswerCount += positiveAnswers.size();
			positiveAnswers.clear();
			firstPerson = true;
		} else if (firstPerson) {
			positiveAnswers.addAll(Arrays.asList(input.split("")));
			firstPerson = false;
		} else {
			List<String> toRemove = new ArrayList<>();
			for (String answer : positiveAnswers) {
				if (!input.contains(answer)) {
					toRemove.add(answer);
				}
			}
			positiveAnswers.removeAll(toRemove);
		}
	}

	@Override
	protected void finishSecondPart() {
		LOG.info("Sum of counts of questions to which everyone answered 'yes' is {0}", positiveAnswerCount);
	}
}
