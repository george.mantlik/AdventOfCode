package cz.mantlik.year_2020;

import cz.mantlik.common.AbstractDay;

import java.util.*;

public class Day1 extends AbstractDay {

	private final Map<Integer, List<Integer>> expenseSums = new HashMap<>();
	private final List<Integer> expenses = new ArrayList<>();

	public Day1() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		int newExpense = Integer.parseInt(input);
		for (Integer expense : expenses) {
			expenseSums.put(expense + newExpense, Arrays.asList(expense, newExpense));
		}
		expenses.add(newExpense);
	}

	@Override
	protected void finishFirstPart() {
		List<Integer> wantedExpenses = expenseSums.get(2020);
		int product = wantedExpenses.get(0) * wantedExpenses.get(1);
		LOG.info("In this list, the two entries that sum to 2020 are {0} and {1}. Multiplying them together produces {2}.",
				wantedExpenses.get(0), wantedExpenses.get(1), product);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Nothing
	}

	@Override
	protected void finishSecondPart() {
		expenseSums.clear();
		for (int i = 0; i < expenses.size(); i++) {
			for (int j = i + 1; j < expenses.size(); j++) {
				for (int k = j + 1; k < expenses.size(); k++) {
					expenseSums.put(expenses.get(i) + expenses.get(j) + expenses.get(k), Arrays.asList(expenses.get(i), expenses.get(j), expenses.get(k)));
				}
			}
		}

		List<Integer> wantedExpenses = expenseSums.get(2020);
		int product = wantedExpenses.get(0) * wantedExpenses.get(1) * wantedExpenses.get(2);
		LOG.info("In this list, the three entries that sum to 2020 are {0}, {1} and {2}. Multiplying them together produces {3}.",
				wantedExpenses.get(0), wantedExpenses.get(1), wantedExpenses.get(2), product);
	}
}
