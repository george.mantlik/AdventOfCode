package cz.mantlik.year_2020;

import cz.mantlik.common.AbstractDay;

import java.util.*;

public class Day7 extends AbstractDay {

	private final Map<String, Bag> bags = new HashMap<>();

	public Day7() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		String[] bagNames = input.split(" bags contain [0-9] | bag, [0-9] | bags, [0-9] | bag\\.| bags\\.");
		Bag parentBag = getBag(bagNames[0]);
		for (int i = 1; i < bagNames.length; i++) {
			String name = bagNames[i];
			Bag bag = getBag(name);
			bag.parents.add(parentBag);
		}
	}

	private Bag getBag(String name) {
		Bag bag = bags.putIfAbsent(name, new Bag(name));
		return bag == null ? bags.get(name) : bag;
	}

	private Set<Bag> findHolders(Bag bag) {
		Set<Bag> holders = new HashSet<>(bag.parents);
		for (Bag parent : bag.parents) {
			holders.addAll(findHolders(parent));
		}
		return holders;
	}

	@Override
	protected void finishFirstPart() {
		Bag shinyGoldBag = getBag("shiny gold");
		LOG.info("The number of bag colors that can eventually contain at least one shiny gold bag is {0}.", findHolders(shinyGoldBag).size());
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	@Override
	protected void processLineSecondPart(String input) {
		String[] bagNames = input.split(" bags contain | bag, | bags, | bag\\.| bags\\.");
		Bag parentBag = getBag(bagNames[0]);
		for (int i = 1; i < bagNames.length; i++) {
			String[] namesWithAmounts = bagNames[i].split(" ", 2);
			if (!namesWithAmounts[0].equals("no")) {
				int amount = Integer.parseInt(namesWithAmounts[0]);
				String name = namesWithAmounts[1];
				Bag bag = getBag(name);
				bag.parents.add(parentBag);
				parentBag.children.put(bag, amount);
			}
		}
	}

	private int countInsides(Bag bag) {
		int count = 1;
		for (Map.Entry<Bag, Integer> child : bag.children.entrySet()) {
			count += countInsides(child.getKey()) * child.getValue();
		}
		return count;
	}

	@Override
	protected void finishSecondPart() {
		Bag shinyGoldBag = getBag("shiny gold");
		LOG.info("A single shiny gold bag must contain {0} other bags.", countInsides(shinyGoldBag) - 1);
	}

	private static class Bag {

		private final String name;
		private final Set<Bag> parents = new HashSet<>();
		private final Map<Bag, Integer> children = new HashMap<>();

		private Bag(String name) {
			this.name = name;
		}

		@Override
		public int hashCode() {
			return Objects.hash(name);
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof Bag)) {
				return false;
			}
			Bag other = (Bag) obj;
			return this.name.equals(other.name);
		}

		@Override
		public String toString() {
			return name;
		}
	}
}
