package cz.mantlik.year_2020;

import cz.mantlik.common.AbstractDay;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Day8 extends AbstractDay {

	private static final String ACC = "acc";
	private static final String JMP = "jmp";
	private static final String NOP = "nop";

	private final List<Instruction> instructions = new ArrayList<>();
	private final List<Instruction> visitedInstructions = new ArrayList<>();
	private long accumulator = 0;
	private int index = 0;

	public Day8() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		String[] inputParts = input.split(" | \\+");
		int commandParam = Integer.parseInt(inputParts[1]);
		instructions.add(new Instruction(inputParts[0], commandParam));
	}

	@Override
	protected void finishFirstPart() {
		runInstructions();
		LOG.info("Immediately before the program would run an instruction a second time, the value in the accumulator is {0}.", accumulator);
	}

	@Override
	protected void setupSecondPart() {
		// Nothing to do
	}

	private void runInstructions() {
		while (index < instructions.size()) {
			Instruction instruction = instructions.get(index);
			if (visitedInstructions.contains(instruction)) {
				break;
			}
			visitedInstructions.add(instruction);
			instruction.run();
		}
	}

	@Override
	protected void processLineSecondPart(String input) {
		// Nothing to do
	}

	@Override
	protected void finishSecondPart() {
		for (Instruction instruction : visitedInstructions.stream().filter(i -> !i.command.equals(ACC)).collect(Collectors.toList())) {
			instruction.swap();
			index = 0;
			accumulator = 0;
			visitedInstructions.clear();
			runInstructions();
			if (index >= instructions.size()) {
				break;
			}
			instruction.swap();
		}

		LOG.info("After the program terminates, the accumulator contains the value {0}.", accumulator);
	}

	private class Instruction {

		private String command;
		private final int commandParam;

		private Instruction(String command, int commandParam) {
			this.command = command;
			this.commandParam = commandParam;
		}

		public void run() {
			switch (command) {
				case ACC:
					accumulator += commandParam;
					index += 1;
					break;
				case JMP:
					index += commandParam;
					break;
				default:
					index += 1;
					break;
			}
		}

		public void swap() {
			if (!command.equals(ACC)) {
				command = command.equals(NOP) ? JMP : NOP;
			}
		}
	}
}
