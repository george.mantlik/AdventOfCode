package cz.mantlik.year_2020;

import cz.mantlik.common.AbstractDay;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day4 extends AbstractDay {

	private final Map<String, String> presentFields = new HashMap<>();
	private int validPassportCount = 0;

	public Day4() {
		super(false);
	}

	@Override
	protected void processLineFirstPart(String input) {
		processLine(input, this::validatePassport);
	}

	private void validatePassport() {
		if (presentFields.size() == 8 || (presentFields.size() == 7 && !presentFields.containsKey("cid"))) {
			validPassportCount += 1;
		}
	}

	@Override
	protected void finishFirstPart() {
		LOG.info("According to the rules, your improved system would report {0} valid passports.", validPassportCount);
	}

	@Override
	protected void setupSecondPart() {
		validPassportCount = 0;
	}

	@Override
	protected void processLineSecondPart(String input) {
		processLine(input, this::validatePassportData);
	}

	private void validatePassportData() {
		int oldValidCount = validPassportCount;
		validatePassport();
		if (validPassportCount > oldValidCount &&
				(byrInvalid() || iyrInvalid() || eyrInvalid() || hgtInvalid() || hclInvalid() || eclInvalid() || pidInvalid())) {
			validPassportCount -= 1;
		}
	}

	private boolean byrInvalid() {
		int byr = Integer.parseInt(presentFields.get("byr"));
		return byr < 1920 || byr > 2002;
	}

	private boolean iyrInvalid() {
		int iyr = Integer.parseInt(presentFields.get("iyr"));
		return iyr < 2010 || iyr > 2020;
	}

	private boolean eyrInvalid() {
		int eyr = Integer.parseInt(presentFields.get("eyr"));
		return eyr < 2020 || eyr > 2030;
	}

	private boolean hgtInvalid() {
		String hgt = presentFields.get("hgt");
		int height;
		if (hgt.contains("cm")) {
			height = Integer.parseInt(hgt.replace("cm", ""));
			return height < 150 || height > 193;
		} else {
			height = Integer.parseInt(hgt.replace("in", ""));
			return height < 59 || height > 76;
		}
	}

	private boolean hclInvalid() {
		return !presentFields.get("hcl").matches("#[0-9a-f]{6}");
	}

	private boolean eclInvalid() {
		return !presentFields.get("ecl").matches("amb|blu|brn|gry|grn|hzl|oth");
	}

	private boolean pidInvalid() {
		return !presentFields.get("pid").matches("[0-9]{9}");
	}

	@Override
	protected void finishSecondPart() {
		finishFirstPart();
	}

	private void processLine(String input, Runnable validationFunction) {
		if (input.isEmpty()) {
			validationFunction.run();
			presentFields.clear();
		} else {
			Map<String, String> fields = Stream.of(input.split(" ")).collect(Collectors.toMap(s -> s.substring(0, 3), s -> s.substring(4)));
			presentFields.putAll(fields);
		}
	}
}
