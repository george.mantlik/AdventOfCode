package cz.mantlik.year_2024;

import cz.mantlik.common.DayTest;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day3Test extends DayTest {

	public Day3Test() {
		super(Day3::new);
	}

	@Test
	@Override
	public void firstPartTest() {
		Object testResult = runTest();
		assertThat(testResult, is(161));
	}

	@Test
	@Override
	public void secondPartTest() {
		Object testResult = runFile("test2");
		assertThat(testResult, is(48));
	}

}