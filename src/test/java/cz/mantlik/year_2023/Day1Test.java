package cz.mantlik.year_2023;

import cz.mantlik.common.DayTest;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day1Test extends DayTest {

	public Day1Test() {
		super(Day1::new);
	}

	@Test
	@Override
	public void firstPartTest() {
		Object testResult = runTest();
		assertThat(testResult, is(142));
	}

	@Test
	@Override
	public void secondPartTest() {
		Object testResult = runFile("test2");
		assertThat(testResult, is(281));
	}

}