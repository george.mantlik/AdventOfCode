package cz.mantlik.year_2023;

import cz.mantlik.common.DayTest;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day10Test extends DayTest {

	public Day10Test() {
		super(Day10::new);
	}

	@Test
	@Override
	public void firstPartTest() {
		Object testResult = runFile("square-loop");
		assertThat(testResult, is(4));
		testResult = runTest();
		assertThat(testResult, is(8));
	}

	@Test
	@Override
	public void secondPartTest() {
		Object testResult = runFile("test-second-part");
		assertThat(testResult, is(4));
		testResult = runFile("test-second-part-larger");
		assertThat(testResult, is(8));
		testResult = runFile("test-second-part-larger-junk");
		assertThat(testResult, is(10));
	}

}