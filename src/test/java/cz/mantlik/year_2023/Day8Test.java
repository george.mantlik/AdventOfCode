package cz.mantlik.year_2023;

import cz.mantlik.common.DayTest;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day8Test extends DayTest {

	public Day8Test() {
		super(Day8::new);
	}

	@Test
	@Override
	public void firstPartTest() {
		Object testResult = runTest();
		assertThat(testResult, is(2L));

		testResult = runFile("test2");
		assertThat(testResult, is(6L));
	}

	@Test
	@Override
	public void secondPartTest() {
		Object testResult = runFile("test-second-part");
		assertThat(testResult, is(6L));
	}

}