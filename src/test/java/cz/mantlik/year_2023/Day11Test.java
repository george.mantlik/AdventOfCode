package cz.mantlik.year_2023;

import cz.mantlik.common.DayTest;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day11Test extends DayTest {

	public Day11Test() {
		super(Day11::new);
	}

	@Test
	@Override
	public void firstPartTest() {
		Object testResult = runTest();
		assertThat(testResult, is(374L));
	}

	@Test
	@Override
	public void secondPartTest() {
		Object testResult = runTest();
		assertThat(testResult, is(82000210L));
	}

}