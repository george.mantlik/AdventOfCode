package cz.mantlik.year_2023;

import cz.mantlik.common.DayTest;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class Day12Test extends DayTest {

	public Day12Test() {
		super(Day12::new);
	}

	@Test
	@Override
	public void firstPartTest() {
		Object testResult = runFile("undamaged-records");
		assertThat(testResult, is(6L));
		testResult = runTest();
		assertThat(testResult, is(21L));
	}

	@Test
	@Override
	public void secondPartTest() {
		Object testResult = runTest();
		assertThat(testResult, is(82000210L));
	}

}