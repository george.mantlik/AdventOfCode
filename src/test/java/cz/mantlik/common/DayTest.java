package cz.mantlik.common;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import cz.mantlik.puzzleutils.PuzzleLogger;

import java.util.function.Function;
import java.util.function.Supplier;

public abstract class DayTest {

	protected static final PuzzleLogger LOG = PuzzleLogger.getLogger(DayTest.class.getName());

	@Rule
	public TestName testName = new TestName();

	private final DayRunner dayRunner;
	private final Supplier<AbstractDayNg> daySupplier;
	private Function<AbstractDayNg, DayPart> dayPartSupplier;

	protected DayTest(Supplier<AbstractDayNg> daySupplier) {
		this.dayRunner = new DayRunner();
		this.daySupplier = daySupplier;
	}

	protected Object runTest() {
		setUpRun("Test");
		return dayRunner.run(true);
	}

	protected Object runFile(String fileName) {
		setUpRun("Test");
		return dayRunner.run(fileName);
	}

	@Before
	public void setUpPart() {
		if(testName.getMethodName().equals("secondPartTest")) {
			dayPartSupplier = AbstractDayNg::getSecondPart;
		} else {
			dayPartSupplier = AbstractDayNg::getFirstPart;
		}
	}

	@After
	public void runMain() {
		setUpRun("Main");
		dayRunner.run(false);
	}

	public abstract void firstPartTest();

	public abstract void secondPartTest();

	private void setUpRun(String header) {
		logHeader(header);
		dayRunner.setDayPart(dayPartSupplier.apply(daySupplier.get()));
	}

	private void logHeader(String header) {
		LOG.info("---------------------- {0} ----------------------", header);
	}
}
